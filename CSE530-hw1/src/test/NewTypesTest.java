package test;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.time.Instant;

import org.junit.Before;
import org.junit.Test;

import hw1.BooleanField;
import hw1.Catalog;
import hw1.Database;
import hw1.DateField;
import hw1.DateTimeField;
import hw1.FloatField;
import hw1.HeapFile;
import hw1.IntField;
import hw1.Query;
import hw1.Relation;
import hw1.StringField;
import hw1.TimeField;
import hw1.Tuple;
import hw1.TupleDesc;
import hw1.Type;



public class NewTypesTest {
	
	private Catalog c;
	private TupleDesc td;
	private HeapFile hf;
	@Before
	public void setup() {
		try {
			Files.copy(new File("testfiles/test.dat.bak").toPath(), new File("testfiles/test.dat").toPath(), StandardCopyOption.REPLACE_EXISTING);
		} catch (IOException e) {
			System.out.println("unable to copy files");
			e.printStackTrace();
		}
		
		c = Database.getCatalog();
		c.loadSchema("testfiles/test.txt");
		
		c = Database.getCatalog();
		td = new TupleDesc(new Type[]{Type.INT, Type.STRING, Type.BOOLEAN,
				Type.FLOAT, Type.DATE, Type.TIME, Type.DATETIME}, new String[]{"a1", "a2",
						"a3", "a4", "a5", "a6", "a7"});
		hf = new HeapFile(new File("testfiles/B.dat"), td);
		c.addTable(hf, "TypeTest");
	}
	
	
	@Test
	public void testWrite() {
		System.out.println("hf.getAllTuples().size() at beginning of testWrite() = " + hf.getAllTuples().size());
		Tuple t = new Tuple(td);
		IntField iif = new IntField(1800);
		t.setField(0, iif.toByteArray());
		StringField sf = new StringField("MY_STRING_FIELD");
		t.setField(1, sf.toByteArray());
		BooleanField bf = new BooleanField(false);
		t.setField(2, bf.toByteArray());
		FloatField ff = new FloatField(87.21f);
		t.setField(3, ff.toByteArray());
		DateField df = new DateField(2015, 3, 22);
		t.setField(4, df.toByteArray());
		TimeField tf = new TimeField(12, 30);
		t.setField(5, tf.toByteArray());
		DateTimeField dtf = new DateTimeField(Instant.now().getEpochSecond());
		t.setField(6, dtf.toByteArray());
		
		Tuple t2 = new Tuple(td);
		iif = new IntField(777);
		t2.setField(0, iif.toByteArray());
		sf = new StringField("Databases are cool!");
		t2.setField(1, sf.toByteArray());
		bf = new BooleanField(false);
		t2.setField(2, bf.toByteArray());
		ff = new FloatField(3.14f);
		t2.setField(3, ff.toByteArray());
		df = new DateField(1997, 4, 7);
		t2.setField(4, df.toByteArray());
		tf = new TimeField(12, 30);
		t2.setField(5, tf.toByteArray());
		dtf = new DateTimeField(Instant.now().getEpochSecond());
		t2.setField(6, dtf.toByteArray());
		
		Tuple t3 = new Tuple(td);
		iif = new IntField(0);
		t3.setField(0, iif.toByteArray());
		sf = new StringField("******");
		t3.setField(1, sf.toByteArray());
		bf = new BooleanField(false);
		t3.setField(2, bf.toByteArray());
		ff = new FloatField(234567.654f);
		t3.setField(3, ff.toByteArray());
		df = new DateField(1997, 4, 7);
		t3.setField(4, df.toByteArray());
		tf = new TimeField(5, 0);
		t3.setField(5, tf.toByteArray());
		dtf = new DateTimeField(Instant.now().getEpochSecond());
		t3.setField(6, dtf.toByteArray());
		
		try {
			hf.addTuple(t);
			hf.addTuple(t2);
			hf.addTuple(t3);
		} catch (Exception e) {
			e.printStackTrace();
			fail("unable to add valid tuple");
		}
		
		System.out.println("hf.getAllTuples().size() at end = " + hf.getAllTuples().size());
		System.out.println("HeapFile tuples -> " + hf.getAllTuples().toString());
		assertTrue(hf.getAllTuples().size() == 3);
	}
	
	@Test
	public void testRemove() {
		System.out.println("hf.getAllTuples().size() at beginning of testRemove() = " + hf.getAllTuples().size());
		Tuple t = new Tuple(td);
		IntField iif = new IntField(530);
		t.setField(0, iif.toByteArray());
		StringField sf = new StringField("MY_STRING_FIELD");
		t.setField(1, sf.toByteArray());
		BooleanField bf = new BooleanField(true);
		t.setField(2, bf.toByteArray());
		FloatField ff = new FloatField(56.78f);
		t.setField(3, ff.toByteArray());
		DateField df = new DateField(1997, 4, 7);
		t.setField(4, df.toByteArray());
		TimeField tf = new TimeField(12, 30);
		t.setField(5, tf.toByteArray());
		DateTimeField dtf = new DateTimeField(1997, 4, 7, 6, 0);
		t.setField(6, dtf.toByteArray());
		
		try {
			hf.addTuple(t);
			System.out.println("Successfully added Tuple t -> " + t);
			hf.deleteTuple(t);
			System.out.println("Successfully removed Tuple t -> " + t);
		} catch (Exception e) {
			e.printStackTrace();
			fail("unable to remove valid tuple");
		}
		
		System.out.println("hf.getAllTuples().size() at end = " + hf.getAllTuples().size());
		System.out.println("HeapFile tuples -> " + hf.getAllTuples().toString());
		assertTrue(hf.getAllTuples().size() == 4);
	}
	
//	@Test
//	public void everythingTest() {
//		Query q = new Query("SELECT a1, a2 AS a3 FROM A JOIN test ON test.c1 = a.a1 WHERE a1 = 530 GROUP BY a1");
//		Relation r = q.execute();
//		
//		assertTrue(r.getTuples().size() == 1);
//	}
//	
	@Test
	public void testSimple() {
		Query q = new Query("SELECT a2, a4, a7 AS Timestamp FROM TypeTest");
		Relation r = q.execute();
		
		assertTrue(r.getTuples().size() == 4);
		assertTrue(r.getDesc().getSize() == 129+4+8);
	}
	
	@Test
	public void testSelect() {
		Query q = new Query("SELECT a2, a7 FROM TypeTest WHERE a1 = 1800");
		Relation r = q.execute();
		System.out.println(r.getTuples().size());
		assert(r.getTuples().size() == 1);
//		System.out.println("r.getDesc().getSize() - > " + r.getDesc().getSize());
		assert(r.getDesc().getSize() == 129+8);
	}
	
	@Test
	public void testProject() {
		Query q = new Query("SELECT a6 FROM TypeTest");
		Relation r = q.execute();
		
		assert(r.getDesc().getSize() == 8);
		assert(r.getTuples().size() == 4);
		assert(r.getDesc().getFieldName(0).equals("a6"));
	}
	
	@Test
	public void testJoin() {
		Tuple t = new Tuple(td);
		IntField iif = new IntField(530);
		t.setField(0, iif.toByteArray());
		StringField sf = new StringField("MY_STRING_FIELD");
		t.setField(1, sf.toByteArray());
		BooleanField bf = new BooleanField(true);
		t.setField(2, bf.toByteArray());
		FloatField ff = new FloatField(56.78f);
		t.setField(3, ff.toByteArray());
		DateField df = new DateField(1997, 4, 7);
		t.setField(4, df.toByteArray());
		TimeField tf = new TimeField(12, 30);
		t.setField(5, tf.toByteArray());
		DateTimeField dtf = new DateTimeField(Instant.now().getEpochSecond());
		t.setField(6, dtf.toByteArray());
		
		try {
			hf.addTuple(t);
		} catch (Exception e) {
			e.printStackTrace();
			fail("unable to remove valid tuple");
		}
		
		Query q = new Query("SELECT c1, c2, a1, a2 FROM test JOIN TypeTest ON test.c1 = TypeTest.a1");
		Relation r = q.execute();
		
		assert(r.getTuples().size() == 5);
		assert(r.getDesc().getSize() == 141);
	}
//	
	@Test
	public void testAggregate() {
		Query q = new Query("SELECT MAX(a7) FROM TypeTest");
		Relation r = q.execute();
		
//		Query q2 = new Query("SELECT COUNT(a2) FROM A");
//		Relation r2 = q2.execute();
		
//		System.out.println(r2.toString());
		
		assertTrue(r.getTuples().size() == 1);
//		IntField agg = new IntField(r.getTuples().get(0).getField(0));
	}
//	
//	@Test
//	public void testGroupBy() {
//		Query q = new Query("SELECT a1, SUM(a2) FROM A GROUP BY a1");
//		Relation r = q.execute();
//		
//		assertTrue(r.getTuples().size() == 4);
//	}
//	
	@Test
	public void testSelectAll() {
		Query q = new Query("SELECT * FROM TypeTest");
		Relation r = q.execute();
		
		assertTrue(r.getTuples().size() == 4);
		assertTrue(r.getDesc().getSize() == 4+129+1+4+8+8+8);
	}
	
	@Test
	public void testAS() {
		Query q = new Query("SELECT a7 AS Timestamp FROM TypeTest ");
		Relation r = q.execute();
		
		assertTrue(r.getDesc().numFields() == 1);
		assertTrue(r.getDesc().getFieldName(0).equals("Timestamp"));
	}
	
	
}
