package test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.time.Instant;
import java.util.Iterator;

import org.junit.Before;
import org.junit.Test;

import hw1.BooleanField;
import hw1.Catalog;
import hw1.Database;
import hw1.DateField;
import hw1.DateTimeField;
import hw1.FloatField;
import hw1.HeapFile;
import hw1.HeapPage;
import hw1.TimeField;
import hw1.Tuple;
import hw1.TupleDesc;

public class HeapPageTest {
	
	private HeapFile hf;
	private TupleDesc td;
	private Catalog c;
	private HeapPage hp;
	
	@Before
	public void setup() {
		
		try {
			Files.copy(new File("testfiles/test.dat.bak").toPath(), new File("testfiles/test.dat").toPath(), StandardCopyOption.REPLACE_EXISTING);
			Files.copy(new File("testfiles/B.dat.bak").toPath(), new File("testfiles/B.dat").toPath(), StandardCopyOption.REPLACE_EXISTING);
		} catch (IOException e) {
			System.out.println("unable to copy files");
			e.printStackTrace();
		}
		
		c = Database.getCatalog();
		c.loadSchema("testfiles/test.txt");
		
		c = Database.getCatalog();
		c.loadSchema("testfiles/B.txt");
		
		int tableId = c.getTableId("B");
		td = c.getTupleDesc(tableId);
		hf = c.getDbFile(tableId);
		hp = hf.readPage(0);
	}
	
	@Test
	public void testGetters() {
		assertTrue(hp.getNumSlots() == 30);
		assertTrue(hp.slotOccupied(0));
		for(int i = 1; i < 30; i++) {
			assertFalse(hp.slotOccupied(i));
		}
	}
	
	@Test
	public void testAddTuple() {
		Tuple t = new Tuple(td);
		t.setField(0, new byte[] {0, 0, 0, (byte)131});
		byte[] s = new byte[129];
		s[0] = 2;
		s[1] = 98;
		s[2] = 121;
		t.setField(1, s);
		BooleanField bf = new BooleanField(true);
		t.setField(2, bf.toByteArray());
		FloatField ff = new FloatField(56.78f);
		t.setField(3, ff.toByteArray());
		DateField df = new DateField(1997, 4, 7);
		t.setField(4, df.toByteArray());
		TimeField tf = new TimeField(12, 30);
		t.setField(5, tf.toByteArray());
		DateTimeField dtf = new DateTimeField(Instant.now().getEpochSecond());
		t.setField(6, dtf.toByteArray());
		
		//System.out.println(t);
		
		try {
			hp.addTuple(t);
		} catch (Exception e) {
			fail("error when adding valid tuple");
			e.printStackTrace();
		}
		
		Iterator<Tuple> it = hp.iterator();
		
		// hp.get
		
		assertTrue(it.hasNext());
		System.out.println(it.next()); // added print
		//assertTrue(it.hasNext());
		//System.out.println(it.next());
		assertFalse(it.hasNext());
//		System.out.println(it.next());
	}
	
	@Test
	public void testDelete() {
		Tuple t = new Tuple(td);
		t.setField(0, new byte[] {0, 0, 0x02, 0x12});
		byte[] s = new byte[129];
		s[0] = 2;
		s[1] = 0x68;
		s[2] = 0x69;
		t.setField(1, s);
		
		try {
			hp.deleteTuple(t);
		} catch (Exception e) {
			fail("error when deleting valid tuple");
			e.printStackTrace();
		}
		
		Iterator<Tuple> it = hp.iterator();
		assertFalse(it.hasNext());
		
		
		
	}

}
