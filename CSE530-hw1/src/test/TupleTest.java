package test;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import hw1.IntField;
import hw1.StringField;
import hw1.Tuple;
import hw1.TupleDesc;
import hw1.Type;

public class TupleTest {

	@Test
	public void testFieldAccess() {
		Type[] t = new Type[] {Type.INT, Type.STRING};//, Type.BOOLEAN, Type.FLOAT};
		String[] c = new String[] {"a", "bs"};
		
		TupleDesc td = new TupleDesc(t, c);
		
		Tuple tup = new Tuple(td);
		
//		byte[] f1 = new byte[] {(byte)(Math.random() * 256), (byte)(Math.random() * 256), (byte)(Math.random() * 256), (byte)(Math.random() * 256)};
//		byte[] f2 = new byte[] {(byte)(Math.random() * 256), (byte)(Math.random() * 256), (byte)(Math.random() * 256), (byte)(Math.random() * 256)};

		IntField intF = new IntField(300);
		StringField sf = new StringField("My_String_Field");
		BooleanField bf = new BooleanField(true);
		FloatField ff = new FloatField(56.78);
		DateField df = new DateField()
		
		tup.setField(0, intF.toByteArray());
		tup.setField(1, sf.toByteArray());
		
		System.out.println(new IntField(f1).toString());
		System.out.println(new StringField(f2).toString());
		
		assertTrue(tup.getField(0).equals(f1));
		assertTrue(tup.getField(1).equals(f2));

	}

}
