package test;

import static org.junit.Assert.*;

import java.util.NoSuchElementException;

import org.junit.Test;

import hw1.TupleDesc;
import hw1.Type;

public class TupleDescTest {
	
	private static final String alphabet = "abcdefghijklmnopqrstuvwxyz0123456789";
	

	private Type[] randomTypes(int n) {
		Type[] t = new Type[n];
		for(int i = 0; i < n; i++) {
			double randomNum = Math.random();
			if(randomNum < 10.0/7.0/10.0) {
				t[i] = Type.INT;
			}
			else if(10.0/7.0/10.0 < randomNum && randomNum < (2)*10.0/7.0/10.0) {
				t[i] = Type.STRING;
			}
			else if((2)*10.0/7.0/10.0 < randomNum && randomNum < (3)*10.0/7.0/10.0) {
				t[i] = Type.BOOLEAN;
			}
			else if((3)*10.0/7.0/10.0 < randomNum && randomNum < (4)*10.0/7.0/10.0) {
				t[i] = Type.FLOAT;
			}
			else if((4)*10.0/7.0/10.0 < randomNum && randomNum < (5)*10.0/7.0/10.0) {
				t[i] = Type.DATE;
			}
			else if((5)*10.0/7.0/10.0 < randomNum && randomNum < (6)*10.0/7.0/10.0) {
				t[i] = Type.TIME;
			}
			else {
				t[i] = Type.DATETIME;
			}
		}
		return t;
	}
	
	private String[] randomColumns(int n) {
		String[] c = new String[n];
		for(int i = 0; i < n; i++) {
			int l = (int)(Math.random() * 12 + 2);
			String s = "";
			for (int j = 0; j < l; j++) {
				s += alphabet.charAt((int)(Math.random() * 36));
			}
			c[i] = s;
		}
		return c;
	}
	
	@Test
	public void testGetType() {
		for(int i = 0; i < 10; i++) {
			int size = (int)(Math.random() * 15 + 1);
			Type[] t = randomTypes(size);
			String[] c = randomColumns(size);
			TupleDesc td = new TupleDesc(t, c);
			for(int j = 0; j < size; j++) {
				assertTrue(td.getType(j) == t[j]);
			}
		}
		
	}
	
	@Test
	public void testNameToId() {
		for(int i = 0; i < 10; i++) {
			int size = (int)(Math.random() * 15 + 1);
			Type[] t = randomTypes(size);
			String[] c = randomColumns(size);
			TupleDesc td = new TupleDesc(t, c);
			for(int j = 0; j < size; j++) {
				assertTrue(td.nameToId(c[j]) == j);
			}
		}
		
		try {
			int size = (int)(Math.random() * 15 + 1);
			Type[] t = randomTypes(size);
			String[] c = randomColumns(size);
			TupleDesc td = new TupleDesc(t, c);
			td.nameToId("");
			fail("found column name that does not exist");
		} catch(NoSuchElementException e) {
			
		}
	}
	
	@Test
	public void testGetSize() {
		Type[] t = {Type.DATE, Type.TIME, Type.DATETIME, Type.BOOLEAN};
		String[] c = {"", "", ""};
		TupleDesc td = new TupleDesc(t, c);
		assertTrue(td.getSize() == 8*3+1);
	}
	
	@Test
	public void testEquals() {
		TupleDesc td1 = new TupleDesc(new Type[]{Type.INT}, new String[]{""});
		TupleDesc td2 = new TupleDesc(new Type[]{Type.INT}, new String[]{""});
		TupleDesc td3 = new TupleDesc(new Type[]{Type.INT, Type.STRING}, new String[]{"", ""});
		
		assertTrue(td1.equals(td2));
		assertTrue(td2.equals(td1));
		assertTrue(td1.equals(td1));
		assertTrue(td2.equals(td2));
		
		assertFalse(td1.equals(td3));
		assertFalse(td2.equals(td3));
		assertFalse(td3.equals(td1));
		
		td1 = new TupleDesc(new Type[]{Type.STRING}, new String[]{""});
		td2 = new TupleDesc(new Type[]{Type.STRING}, new String[]{""});
		td3 = new TupleDesc(new Type[]{Type.INT, Type.STRING}, new String[]{"", ""});
		
		assertTrue(td1.equals(td2));
		assertTrue(td2.equals(td1));
		assertTrue(td1.equals(td1));
		assertTrue(td2.equals(td2));
		
		assertFalse(td1.equals(td3));
		assertFalse(td2.equals(td3));
		assertFalse(td3.equals(td1));
		
		
	}

}
