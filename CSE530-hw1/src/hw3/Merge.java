package hw3;

import hw1.Field;

public class Merge {
	public final Field newKey;
	public final Field oldKey;
	public final Node left;
	public final Node right;
	
	public Merge(Field newKey, Field oldKey, Node left, Node right) {
		this.newKey = newKey;
		this.oldKey = oldKey;
		this.right = right;
		this.left = left;
	}
	
	public void dump() {
		if (this.left != null) {
			this.left.dump();
		}
		System.out.print(">> " + this.newKey + "\n");
		if (this.right != null) {
			this.right.dump();
			}  
	}
}
