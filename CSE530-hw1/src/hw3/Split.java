package hw3;

import hw1.Field;

public class Split {

	public final Field key;
	public final Node left;
	public final Node right;
	
	public Split(Field key, Node left, Node right) {
		this.key = key;
		this.right = right;
		this.left = left;
	}
	
	public void dump() {
		this.left.dump();
		System.out.print(">> " + this.key + "\n");
		this.right.dump();  
	}
	
}
