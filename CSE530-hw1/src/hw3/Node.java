package hw3;

import java.util.ArrayList;

import hw1.Field;

public interface Node {
	
	 public int search(Field key);
	 public Split insert(Field key, Entry value);
	 public void dump();
	 public boolean isLeafNode();
	 public void setParent(Node node);
	 public boolean testMinimalSize();
	 public Merge delete(Field key);
	 public Node getNearestSibling(Field key);
	 public int capacity();
	 public Node mergeWith(Node sibling, Field oldKey);
	 public void setAsRoot();
	 public ArrayList<Field> getKeys();
//	 public Field transferFromSibling();
//	 public Node getFirstNodePointer();
//	 public Node getLastNodePointer();
	 
	 
	
//	public int getDegree();
//	public boolean isLeafNode();
//	public boolean isUnderCapacity();
//	public boolean isOverCapacity();
//	public boolean willBeUnderCapacity();
//	public boolean willBeOverCapacity();
//	public void emptyNode();
//	public int capacity();
//	public Field getLargestKeyValue();
//	public Field getSmallestKeyValue();
//	public Field getPreviousPointer(Entry key);
//	public Field getPreviousPointer(Field key);
//	public Field getNextPointer(Entry key);
//	public Field getNextPointer(Field key);
//	public boolean equals(Node other);
//	public void deleteEntry(int i);
//	public void deleteEntry(Entry e);
//	public void addKeyAndChild(Field f, Node node, boolean willSort);
	
	
	
}
