package hw3;

import java.util.Comparator;

import hw1.Field;
import hw1.RelationalOperator;

public class Entry implements Comparable<Entry> {

	private Field f;
	private int page;
	
	public Entry(Field f, int page) {
		this.f = f;
		this.page = page;
	}
	
	public Field getField() {
		return this.f;
	}
	
	public int getPage() {
		return this.page;
	}

	@Override
	public int compareTo(Entry o) {
		if (this.f.compare(RelationalOperator.LT, o.getField())) {
			return -1;
		}
		else if (this.f.compare(RelationalOperator.EQ, o.getField())) {
			return 0;
		}
		else {
			return 1;
		}
	}
	
//	public static class Comparators {
//	    public static final Comparator<Entry> FIELD = (Entry e1, Entry e2) -> {
//	    	if (e1.getField().compare(RelationalOperator.LT, e2.getField())) {
//	    		return -1;
//	    	}
//	    	else
//	    }
//	}
}
