package hw3;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;

import hw1.Field;
import hw1.RelationalOperator;

public class InnerNode implements Node {
	
	private int degree;
	private InnerNode parent;
	private ArrayList<Field> keys;
	private ArrayList<Node> children;
	private boolean foundRightSibling;
	private boolean isRoot;
	
	public InnerNode(int degree) {
		this.degree = degree;
		this.parent = null;
		this.keys = new ArrayList<Field>();
		this.children = new ArrayList<Node>();
		this.foundRightSibling = false;
		this.isRoot = false;
	}

	@Override
	public int search(Field key) {
		System.out.println("Inner Node Search Key being compared: " + key + " <= " + this.keys.get(0));
		if (key.compare(RelationalOperator.LTE, this.keys.get(0))) {
			return 0;
		}
		System.out.println("Inner Node Search Key being compared: " + key + " > " + this.keys.get(this.keys.size()-1));
		if (key.compare(RelationalOperator.GT, this.keys.get(this.keys.size()-1))) {
			return this.keys.size(); // return last child index
		}
		for (int i = 1; i < this.keys.size(); i++) {
			System.out.println("Inner Node Search Key being compared: " + this.keys.get(i-1) + " < " + key + " <= " + this.keys.get(i));
			if (key.compare(RelationalOperator.GT, this.keys.get(i-1)) && key.compare(RelationalOperator.LTE, this.keys.get(i))) {
				return i;
			}
		}
		return -1;
	}

	@Override
	public Split insert(Field key, Entry value) {
		Node nextNode = this.getChild(key);
		nextNode.setParent(this);
		Split split = nextNode.insert(key, value);
		if (split != null) { // Handle a split
//			int indexToInsert = this.search(split.key);
//			System.out.println("split.key to be added at index " + indexToInsert + " in current inner node");
			System.out.print("Current ");
			this.dump();
			this.insertSplit(split);
			if (this.keys.size() > degree) {
				// Create a Sibling for the split
				System.out.println("InnerNode FULL . . . . . Splitting InnerNode");
				System.out.println("Degree - " + degree);
				System.out.println("thisInnerNode.keys -> " + this.keys);
				InnerNode sibling = new InnerNode(degree);
				Field newParentKey = null;
				if (degree % 2 == 0) { // Split on different index for even and odd degree trees
					sibling.keys = new ArrayList<Field>(this.keys.subList((int) Math.floor((degree+2)/2.0), degree+1));
					newParentKey = this.keys.get((int) Math.floor((degree+1)/2.0));
					sibling.children = new ArrayList<Node>(this.children.subList((int) Math.floor((degree+2)/2.0), degree+2));
					this.keys.subList((int) Math.floor((degree+1)/2.0), degree+1).clear();
					this.children.subList((int) Math.floor((degree+2)/2.0), degree+2).clear();
				}
				else {
					sibling.keys = new ArrayList<Field>(this.keys.subList((int) Math.floor((degree+3)/2.0), degree+1));
					newParentKey = this.keys.get((int) Math.floor((degree+1)/2.0));
					sibling.children = new ArrayList<Node>(this.children.subList((int) Math.floor((degree+3)/2.0), degree+2));
					this.keys.subList((int) Math.floor((degree+1)/2.0), degree+1).clear();
					this.children.subList((int) Math.floor((degree+3)/2.0), degree+2).clear();
				}
				this.isRoot = false; // if this node was the leaf node, let it know it isn't anymore
				System.out.println("Original inner node's new keys ->" + this.keys);
				System.out.println("Original inner node's new children -> " + this.toString());
				System.out.println("New Split's key value -> " + newParentKey);
				System.out.println("New sibling's new keys -> " + sibling.keys);
				System.out.println("New sibling's children -> " + sibling.toString());
				split = new Split(newParentKey, this, sibling); 
				return split;
			}
			else {
				return null;
			}
		}
		return split;
	}
	
	public void insertSplit(Split split) {
		if (this.keys.isEmpty()) {
			System.out.println("Adding split.key ( " + split.key + " ) to pos 0 of empty inner node");
			this.addKey(split.key); // actual line of code (everything else is testing purposes)
			System.out.println("Adding split.left");
			split.left.dump();
			System.out.println("to child 0 of empty inner node");
			this.addChild(split.left); // actual line of code (everything else is testing purposes)
			System.out.println("Adding split.right");
			split.right.dump();
			System.out.println("to child 1 of empty inner node");
			this.addChild(split.right); // actual line of code (everything else is testing purposes)
			System.out.println("Split key added to pos 0 because of empty inner node");
			return;
		}
		if (split.key.compare(RelationalOperator.LTE, this.keys.get(0))) {
			System.out.println("Adding split.key ( " + split.key + " ) to pos 0,"
					+ " shifting key 0 ( " + this.keys.get(0) + " ) over to key 1");
			this.addKey(0, split.key); // actual line of code (everything else is testing purposes)
			System.out.println("Setting split.left");
			split.left.dump();
			System.out.println("to child 0, replacing current child 0");
			this.children.get(0).dump();
			this.setChild(0, split.left); // actual line of code (everything else is testing purposes)
			System.out.println("Setting split.right");
			split.right.dump();
			System.out.println("to child 1, displacing current child 1 to child 2");
			this.children.get(1).dump();
			this.addChild(1, split.right); // actual line of code (everything else is testing purposes)
			System.out.println("Split key added to inner node key 0 because of smallest key");
			return;
		}
		if (split.key.compare(RelationalOperator.GT, this.keys.get(this.keys.size()-1))) {
			System.out.println("Adding new largest key value split.key ( " + split.key + " ),"
					+ " after current largest key ( " + this.keys.get(this.keys.size()-1) + " )");
			this.addKey(split.key);
			System.out.println("Setting split.left");
			split.left.dump();
			System.out.println("to child " + (this.children.size()-1) + ", replacing current child");
			this.children.get(this.children.size()-1).dump();
			this.setChild(this.children.size()-1, split.left); // actual line of code (everything else is testing purposes)
			System.out.println("Creating split.right");
			split.right.dump();
			System.out.println("as child " + (this.children.size()));
			this.addChild(split.right); // actual line of code (everything else is testing purposes)
			System.out.println("Split key added as inner node key " + (this.keys.size()) + " because of new largest key");
			return;
		}
		for (int i = 1; i < this.children.size(); i++) {
			if (split.key.compare(RelationalOperator.GT, this.keys.get(i-1)) && split.key.compare(RelationalOperator.LTE, this.keys.get(i))) { // actual line of code (everything else is testing purposes)
				System.out.println("Inserting new key ( " + split.key + " ),"
						+ " between key-1=" + this.keys.get(i-1) + " and key=" + this.keys.get(i));
				this.addKey(i, split.key); // actual line of code (everything else is testing purposes)
				System.out.println("Setting split.left");
				split.left.dump();
				System.out.println("to child " + i + ", replacing current child");
				this.children.get(i).dump();
				this.setChild(i, split.left); // actual line of code (everything else is testing purposes)
				System.out.println("Adding split.right");
				split.right.dump();
				System.out.println("as child " + (i+1) + ", displacing current child " + (i+1) + " to " + (i+2));
				this.children.get(i+1).dump();
				this.addChild(i+1, split.right); // actual line of code (everything else is testing purposes)
				System.out.println("Split key added to pos " + i + " in inner node");
				return;
			}
		}
	}
	
	public Merge delete(Field key) {
		Node nextNode = this.getChild(key);
		nextNode.setParent(this);
		Merge merge = nextNode.delete(key);
		if (merge != null) { // Handle Merge
			System.out.println("merge != null -- dumping current node . . .");
			this.dump();
			int indexToDelete = this.search(merge.newKey);
			// System.out.println("indexToDelete=" + indexToDelete);
			Field oldKey = this.getParentKey();
			//this.deleteAtIndex(indexToDelete, merge);
			Field newKey = null;
			if (!this.isRoot) {
				System.out.println("Checking this node's capacity");
				InnerNode sibling = (InnerNode) this.getNearestSibling(key);
				System.out.println("Underflow check: " + this.keys.size() + " <= " + ((int)Math.ceil((degree+1)/2.0)));
				if (this.checkUnderflow()) { // check this inner node for under capacity
					System.out.println("Sibling capacity check: " + sibling.capacity() + " > " + ((int)Math.ceil((degree+1)/2.0)));
					if (sibling.capacity() > Math.ceil((degree+1)/2.0)) {
						System.out.println("Taking entry from sibling");
						if (this.foundRightSibling) { // Taking Entry from right sibling
							newKey = sibling.transferKeyLeft(this);
							System.out.println("New key=" + newKey);
							System.out.println("merge.left=" + this);
							System.out.println("merge.right=" + sibling);
							merge = new Merge(newKey, oldKey, this, sibling);
						}
						else { // Taking Entry from left sibling
							newKey = sibling.transferKeyRight(this);
							System.out.println("New key=" + newKey);
							System.out.println("merge.left=" + sibling);
							System.out.println("merge.right=" + this);
							merge = new Merge(newKey, sibling.getParentKey(), sibling, this);
						}
					}
					else { // Nodes have to be merged
						System.out.println(" \n# # # Merging Inner Nodes # # #");
						InnerNode newMergedNode = (InnerNode) this.mergeWith(sibling, oldKey);
						if (this.foundRightSibling) {
							newKey = newMergedNode.getParentKey();
							merge = new Merge(newKey, oldKey, newMergedNode, null); // Case 2: Under capacity node was right node
						}
						else {
							newKey = newMergedNode.getParentKey();
							merge = new Merge(newKey, sibling.getParentKey(), null, newMergedNode); // Case 3: Under capacity node was left node
						}
					}
				}
				else if (indexToDelete == this.keys.size()) {
					newKey = this.getParentKey();
					if (this.foundRightSibling) {
						merge = new Merge(newKey, oldKey, this, sibling);
					}
					else {
						merge = new Merge(newKey, sibling.getParentKey(), sibling, this);
					}
					System.out.println("Replacing key=" + this.keys.get(indexToDelete) + " with key=" + merge.newKey);
					this.replaceKey(indexToDelete, merge.newKey);
				}
			}
			else {
				System.out.println("\nThis node is the root, capacity check skipped");
				merge = new Merge(null, null, null, this);
			}
			System.out.println("Merge object being returned . . .");
			if (merge != null) { 
				merge.dump(); 
			}
			else { 
				System.out.println("merge is null");
			}
			return merge;
		}
		return null;
	}
	
	public void deleteAtIndex(int index, Merge merge) {
		System.out.println("Deleting from inner node=" + this.toString());
		if (index != -1) {
			System.out.println();
			if (this.keys.size()-index-1 < 1) { // Key did not need to be passed up
				// Two merged nodes exist as two farthest right children of the parent node
				System.out.println("Removed key=" + this.keys.remove(this.keys.size()-1));
				System.out.println("Removed right child=" + this.children.remove(this.children.size()-1));
				System.out.println("Removed left child=" + this.children.remove(this.children.size()-1));
				if (merge.left != null && merge.right != null) {
					this.children.add(index, merge.left);
					System.out.println("Added merge.left=" + merge.left + " at index=" + index);
					this.children.add(index, merge.right);
					System.out.println("Added merge.right=" + merge.right + " at index=" + index);
				}
				else if (merge.right != null) {
					this.children.add(merge.right);
					System.out.println("Added merge.right=" + merge.right + " at index=" + index);
				}
				else if (merge.left != null) {
					this.children.add(merge.left);
					System.out.println("Added merge.left=" + merge.left + " at index=" + index);
				}
			}
			else {
				System.out.println("Replaced key=" + this.keys.get(index) + " with merge.newKey=" + merge.newKey);
				this.replaceKey(index, merge.newKey);
				System.out.println("Removed left child=" + this.children.remove(index));
				System.out.println("Removed right child=" + this.children.remove(index));
				if (merge.left != null && merge.right != null) {
					this.children.add(index, merge.left);
					System.out.println("Added merge.left=" + merge.left + " at index=" + index);
					this.children.add(index, merge.right);
					System.out.println("Added merge.right=" + merge.right + " at index=" + index);
				}
				else if (merge.right != null) {
					this.children.add(index, merge.right);
					System.out.println("Added merge.right=" + merge.right + " at index=" + index);
				}
				else if (merge.left != null) {
					this.children.add(index, merge.left);
					System.out.println("Added merge.left=" + merge.left + " at index=" + index);
				}
			}
		}
	}
	
	public void replaceKey(int index, Field key) {
		System.out.println("Inserting key=" + key + " before key=" + this.keys.get(index));
		this.keys.add(index, key);
		this.keys.remove(this.keys.size()-1);
	}
	
	public Node getNearestSibling(Field key) {
		int thisIndexInParent = this.parent.search(key);
		System.out.println("\nthisIndexInParent=" + thisIndexInParent);
		if (thisIndexInParent <= 0) {
			this.foundRightSibling = true;
			System.out.println("Right sibling found -- only sibling available\n");
			return this.parent.getNextPointer(thisIndexInParent); // Right Sibling
		}
		else if (thisIndexInParent >= degree+1) {
			this.foundRightSibling = false;
			System.out.println("Left sibling found -- only sibling available\n");
			return this.parent.getPreviousPointer(thisIndexInParent); // Left Sibling
		}
		else if (thisIndexInParent != -1) {
			Node leftSibling = this.parent.getPreviousPointer(thisIndexInParent); // Left Sibling
			Node rightSibling = this.parent.getNextPointer(thisIndexInParent); // Right Sibling
			if (rightSibling.capacity() >= leftSibling.capacity()) {
				this.foundRightSibling = true;
				System.out.println("Right sibling found -- had more entries than left sibling\n");
				return rightSibling;
			}
			else {
				this.foundRightSibling = false;
				System.out.println("Left sibling found -- had more entries than right sibling\n");
				return leftSibling;
			}
		}
		System.out.println("ERROR: Could not find index in parent node.");
		return null;
	}
	
	public Field transferKeyLeft(InnerNode sibling) {
		Field newKey = null;
		if (this.keys.get(0).compare(RelationalOperator.GT, sibling.keys.get(sibling.keys.size()-1))) {
			newKey = this.keys.get(0);
			int thisIndexInParent = this.getIndexInParent(newKey);
			Field keyInParent = this.parent.getKey(thisIndexInParent);
			if (thisIndexInParent == 0) {
				this.parent.setKey(thisIndexInParent, newKey);
			}
			else {
				this.parent.setKey(thisIndexInParent-1, newKey);
			}
			sibling.addKey(keyInParent);
			sibling.addChild(this.children.get(0));
			System.out.println("Shifted key=" + newKey + " to parent node");
			System.out.println("Shifted key=" + keyInParent + " to left node");
			System.out.println("Shifted child=" + this.children.get(0) + " to left node");
			this.keys.remove(newKey);
			this.children.remove(this.children.get(0));
		}
		else {
			System.out.println("ERROR: Tree is no longer sorted.");
			System.exit(0); // Crash
		}
		return newKey;
	}
	
	public Field transferKeyRight(InnerNode sibling) {
		Field newKey = null;
		if (this.keys.get(this.keys.size()-1).compare(RelationalOperator.LT, sibling.keys.get(0))) {
			newKey = this.keys.get(this.keys.size()-1);
			int thisIndexInParent = this.getIndexInParent(newKey);
			Field keyInParent = this.parent.getKey(thisIndexInParent);
			if (thisIndexInParent == 0) {
				this.parent.setKey(thisIndexInParent, newKey);
			}
			else {
				this.parent.setKey(thisIndexInParent-1, newKey);
			}
			sibling.addKey(0, keyInParent);
			sibling.addChild(0, this.children.get(this.children.size()-1));
			this.keys.remove(newKey);
			System.out.println("Shifted key=" + newKey + " to left node");
			System.out.println("Shifted child=" + this.children.get(this.children.size()-1) + " to left node");
			this.children.remove(this.children.get(this.children.size()-1));
		}
		else {
			System.out.println("ERROR: Tree is no longer sorted.");
			System.exit(0); // Crash
		}
		return newKey;
	}
	
	// When sibling is over capacity, take key from sibling and "push through"
	// When sibling is at minimum capacity, merge nodes and delete level
	public Node mergeWith(Node sibling, Field oldKey) {
		InnerNode innerSibling = (InnerNode) sibling;
		InnerNode newMergedNode = new InnerNode(degree);
		ArrayList<Field> newMergedKeys = new ArrayList<Field>();
		ArrayList<Node> newMergedChildren = new ArrayList<Node>();
		if (this.foundRightSibling) {
			int indexInParent = this.parent.search(oldKey);
			newMergedKeys.addAll(this.keys);
			newMergedKeys.add(this.parent.getKey(indexInParent));
			newMergedKeys.addAll(innerSibling.keys);
			newMergedChildren.addAll(this.children);
			newMergedChildren.addAll(innerSibling.children);
		}
		else {
			int indexInParent = this.parent.search(oldKey);
			newMergedKeys.addAll(innerSibling.keys);
			newMergedKeys.add(this.parent.getKey(indexInParent));
			newMergedKeys.addAll(this.keys);
			newMergedChildren.addAll(innerSibling.children);
			newMergedChildren.addAll(this.children);
		}
		newMergedNode.setKeys(newMergedKeys);
		newMergedNode.setChildren(newMergedChildren);
		return newMergedNode;
	}
	
	public int getIndexInParent(Field key) {
		return this.parent.search(key);
	}
	
	public Node getPreviousPointer(int index) {
		return this.children.get(index-1);
	}
	
	public Node getNextPointer(int index) {
		return this.children.get(index+1);
	}
	
	public int capacity() {
		return this.children.size();
	}
	
	public void addKey(int index, Field key) {
		this.keys.add(index, key);
	}
	
	public void addKey(Field key) {
		this.keys.add(key);
	}
	
	public void setKey(int index, Field key) {
		this.keys.set(index, key);
	}
	
	public void setKeys(ArrayList<Field> newKeys) {
		this.keys = newKeys;
	}
	
	public void addChild(int index, Node node) {
		this.children.add(index, node);
	}
	
	public void removeKeyAndChild(int index) {
		if (index == this.keys.size()) {
			this.keys.remove(index-1);
			this.children.remove(index);
		}
		else if (index == 0) {
			this.keys.remove(index);
			this.children.remove(index);
		}
		else {
			this.keys.remove(index);
			this.children.remove(index+1);
		}
	}
	
	public void addChild(Node node) {
		this.children.add(node);
	}
	
	public void setChild(int index, Node node) {
		this.children.set(index, node);
	}
	
	public void setChildren(ArrayList<Node> newChildren) {
		this.children = newChildren;
	}

	@Override
	public void dump() {
		System.out.println("InnerNode h==?");
	    for (int i = 0; i < this.keys.size(); i++){
			this.children.get(i).dump();
			System.out.print(">> ");
			System.out.println(this.keys.get(i));
	    }
	    this.children.get(this.children.size()-1).dump();
	}
	
	public boolean isLeafNode() {
		return false;
	}
	
	public Field getKey(int index) {
		return this.keys.get(index);
	}
	
	public Node getChild(Field key) {
		System.out.println("Inner Node Key being compared: " + key + " <= " + this.keys.get(0));
		if (key.compare(RelationalOperator.LTE, this.keys.get(0))) {
			return this.children.get(0);
		}
		System.out.println("Inner Node Key being compared: " + key + " > " + this.keys.get(this.keys.size()-1));
		if (key.compare(RelationalOperator.GT, this.keys.get(this.keys.size()-1))) {
			return this.children.get(this.children.size()-1);
		}
		for (int i = 1; i < this.keys.size(); i++) {
			System.out.println("Inner Node Key being compared: " + this.keys.get(i-1) + " < " + key + " <= " + this.keys.get(i));
			if (key.compare(RelationalOperator.GT, this.keys.get(i-1)) && key.compare(RelationalOperator.LTE, this.keys.get(i))) {
				return this.children.get(i);
			}
		}
		return null;
	}
	
	public void setParent(Node node) {
		this.parent = (InnerNode) node;
	}
	
	public ArrayList<Field> getKeys() {
		return this.keys;
	}
	
	public Field getParentKey() {
		return this.keys.get(this.keys.size()-1);
	}
	
	public ArrayList<Node> getChildren() {
		return this.children;
	}
	
	public String toString() {
		return this.keys.toString();
	}

	@Override
	public boolean testMinimalSize() {
		return this.children.size() <= Math.ceil((degree+1)/2.0); // Wikipedia Table
	}

	@Override
	public void setAsRoot() {
		this.isRoot = true;
	}
	
	public boolean checkUnderflow() {
		if (this.degree % 2 == 0) {
			System.out.println("Underflow check: " + this.keys.size() + " < " + ((int)Math.ceil((degree+1)/2.0)-1));
			return this.children.size() < Math.ceil((degree+1)/2.0);
		}
		else {
			System.out.println("Underflow check: " + this.keys.size() + " <= " + ((int)Math.ceil((degree+1)/2.0)-1));
			return this.children.size() <= Math.ceil((degree+1)/2.0);
		}
	}
	
}
//	private int degree;
////	private HashMap<Field, Node> children;
//	private ArrayList<Node> children;
//	private ArrayList<Field> keys;
//	
//	public InnerNode(int degree) {
//		this.degree = degree;
////		this.children = new ArrayList<Node>();
//		this.children = new HashMap<Field,Node>();
//		this.keys = new ArrayList<Field>();
//		//your code here
//	}
//	
//	public InnerNode(InnerNode in) {
//		this.degree = in.degree;
//		this.children = in.children;
//		this.keys = in.keys;
//		//your code here
//	}
//	
//	public InnerNode(InnerNode in, boolean deepcopy) {
//		this.degree = in.degree;
//		this.children = new HashMap<Field, Node>(in.children);
//		this.keys = new ArrayList<Field>(in.keys);
//		//your code here
//	}
//	
//	/*
//	 * Source : http://jxlilin.blogspot.com/2013/11/b-tree-implementation-in-java.html
//	 * getChild(int index)
//	 * setChild(int index, Node n)
//	 * isLeafNode()
//	 * search(Node n)
//	 * insertAt(int index, Entry e)
//	 * delete(Entry e)
//	 * deleteAt(Entry e)
//	 */
//	
//	
//	//we have to fix this to work on any node
//	//if inner node do this method, if leaf node idk what to have it do
//	//maybe add a bplustree input so we can check if its root
//	public void addKeyAndChild(Field f, Node node, boolean willSort) {
//		if (keys.isEmpty()) {
//			this.keys.add(0, f);
//			this.children.put(f, node);
//			return;
//		}
//		if (f.compare(RelationalOperator.LTE, keys.get(0))) {
//			this.keys.add(0, f);
//			this.children.put(f, node);
//			return;
//		}
//		// If the search key is larger than the largest inner node key value we can
//		// set the next node to be the last child and be done with this iteration
//		if (f.compare(RelationalOperator.GT, keys.get(keys.size()-1))) {
//			this.keys.add(keys.size()-1, f);
//			this.children.put(f, node);
//			return;
//		}
//		// Otherwise check if the value is in between the last key value and the current key value
//		for (int i = 1; i < keys.size(); i++) {
//			if (f.compare(RelationalOperator.GT, keys.get(i-1)) && f.compare(RelationalOperator.LTE, keys.get(i))) {
//				this.keys.add(i, f);
//				this.children.put(f, node);
//				return;
//			}
//		}
//		if (willSort) {
//			sortKeys();
//		}
//		return;
//	}
//	
//	public void changeKey(int index, Field f) {
//		this.keys.set(index, f);
//		Node storedNode = this.children.remove(f);
//		this.children.put(f, storedNode);
//		return;
//	}
//	
//	public void addMultipleKeys(ArrayList<Field> keyArrayList, HashMap<Field,Node> keyNodeMap, boolean willSort) {
//		this.keys = new ArrayList<Field>(keyArrayList);
//		this.children = new HashMap<Field,Node>(keyNodeMap);
//		if (willSort) {
//			sortKeys();
//		}
//		return;
//	}
//	
//	public boolean isUnderCapacity() {
//		return ((keys.size() < degree/2) && (children.size() < (degree)/2)) ? true : false;
//	}
//	
//	public boolean isOverCapacity() {
//		return ((keys.size() > degree) && (children.size() > degree)) ? true : false;
//	}
//	
//	public boolean willBeUnderCapacity() {
//		return ((keys.size()-1 < degree/2) && (children.size()-1 < (degree)/2)) ? true : false;
//	}
//	
//	public boolean willBeOverCapacity() {
//		return ((keys.size()+1 > degree) && (children.size()+1 > degree)) ? true : false;
//	}
//	
//	public void emptyNode() {
//		this.keys.clear();
//		this.children.clear();
//		return;
//	}
//	
//	public ArrayList<Field> getKeys() {
//		//your code here
//		return this.keys;
//	}
//	
//	public void sortKeys() {
//		Collections.sort(this.keys, new Comparator<Field>(){
//			public int compare(Field f1,Field f2){
//				if (f1.compare(RelationalOperator.LT, f2)) {
//					return -1;
//				} else if (f1.compare(RelationalOperator.EQ, f2)){
//					return 0;
//				} else {
//					return 1;
//				}
//          }});
//	}
//	
//	public ArrayList<Node> getChildren() {
//		//your code here
//		return new ArrayList<Node>(this.children.values());
//	}
//	
//	public HashMap<Field, Node> getKeyNodeMap() {
//		return this.children;
//	}
//
//	public Field getSmallestKeyValue() {
//		return this.keys.get(0);
//	}
//	
//	public Field getLargestKeyValue() {
//		return this.keys.get(this.keys.size()-1);
//	}
//	
//	public Field getPreviousPointer(Entry key) {
//		int index = this.keys.indexOf(key.getField());
//		return this.keys.get(index-1);
//	}
//	
//	public Field getPreviousPointer(Field key) {
//		int index = this.keys.indexOf(key);
//		return this.keys.get(index-1);
//	}
//	
//	public Field getNextPointer(Entry key) {
//		int index = this.keys.indexOf(key.getField());
//		return this.keys.get(index-1);
//	}
//	
//	public Field getNextPointer(Field key) {
//		int index = this.keys.indexOf(key);
//		return this.keys.get(index-1);
//	}
//		
//	public Pair<Field,Node> findNextNodeForKey(Entry key) {
//		// If the search key is smaller than the smallest inner node key value we can
//		// set the next node to be the first child and be done with this iteration
//		if (key.getField().compare(RelationalOperator.LTE, keys.get(0))) {
//			return new Pair<Field,Node>(keys.get(0), children.get(0));
//		}
//		// If the search key is larger than the largest inner node key value we can
//		// set the next node to be the last child and be done with this iteration
//		if (key.getField().compare(RelationalOperator.GT, keys.get(keys.size()-1))) {
//			return new Pair<Field,Node>(keys.get(keys.size()-1), children.get(children.get(children.size()-1)));
//		}
//		// Otherwise check if the value is in between the last key value and the current key value
//		for (int i = 1; i < keys.size(); i++) {
//			if (key.getField().compare(RelationalOperator.GT, keys.get(i-1)) && key.getField().compare(RelationalOperator.LTE, keys.get(i))) {
//				return new Pair<Field,Node>(keys.get(i), children.get(keys.get(i)));
//			}
//		}
//		return new Pair<Field,Node>(null, null);
//	}
//	
//	public Node getChildForEntry(Entry e) {
//		return this.children.get(e.getField());
//	}
//	
//	public Node getChildForKey(Field key) {
//		return this.children.get(key);
//	}
//	
//	public Node getChildFor(Entry e) {
//		return this.children.get(e.getField());
//	}
//	
//	public int getDegree() {
//		//your code here
//		return this.degree;
//	}
//	
//	public Field getKey(int i) {
//		return this.keys.get(i);
//	}
//	
//	public boolean isLeafNode() {
//		return false;
//	}
//
//	@Override
//	public boolean equals(Node other) {
//		InnerNode otherInner = (InnerNode) other;
//		if (this.degree != otherInner.getDegree()) {
//			return false;
//		}
//		for (int i = 0; i < this.keys.size();i++) {
//			if (this.keys.get(i).compare(RelationalOperator.EQ, otherInner.getKey(i)) && this.children.get(this.keys.get(i)).equals(otherInner.getChildForKey(otherInner.getKey(i)))) {
//				return false;
//			}
//		}
//		return true;
//	}
//	
//	public void deleteEntry(int i) {
//		Field removedkey = this.keys.remove(i);
//		this.children.remove(removedkey);
//		return;
//	}
//	
//	public void deleteEntry(Entry e) {
//		this.keys.remove(e.getField());
//		this.children.remove(e.getField());
//		return;
//	}
//	
//	public int capacity() {
//		return this.keys.size();
//	}
//
//}