package hw3;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.Stack;

import hw1.Field;
import hw1.RelationalOperator;
import test.Pair;

public class BPlusTree {
	private final int degree;
	private Node root;
	private int height;
	
	
	public BPlusTree(int degree) {
		this.degree = degree;
		this.root = new LeafNode(degree, true);
		this.height = 1;
	}
	
	public LeafNode search(Field key) {
		return searchHelper(key, this.root);
	}
	
	private LeafNode searchHelper(Field key, Node node) {
		if (node.isLeafNode()) {
			LeafNode ln = (LeafNode) node;
			if(ln.search(key) != -1){
				return ln;
			}
			else {
				return null;
			}
		}
		else { // Now we know its an inner node, keep looking through children
			InnerNode innerNode = (InnerNode) node; 
			return searchHelper(key, innerNode.getChild(key));
		}
	}
	
	public void insert(Entry value) {
		Field key = value.getField();
		if (this.search(key) == null) { // this is inefficient because you're searching twice but im lazy
			System.out.println("\n * * * NEW INSERT * * *\nkey=" + key);
			Split split = this.root.insert(key, value);
			if (split != null) {
				InnerNode newRoot = new InnerNode(degree);
				newRoot.insertSplit(split);
				this.root = newRoot;
			}
		}
		else {
			System.out.println("ERROR: Entry already exists in tree!");
		}
		this.root.setAsRoot();
	}
	
	public void delete(Entry value) {
		Field key = value.getField();
		if (this.search(key) != null) {
			System.out.println("\n ______ NEW DELETE ______\nkey=" + key);
			Merge merge = this.root.delete(key);
			if (merge != null) { 
				System.out.println("Merge is not null");
				if (merge.newKey == null && merge.oldKey == null && merge.left == null) {
					this.root = merge.right;
				}
				else if (merge.left != null && merge.right != null) {
					System.out.println("merge has two children, same root remains");
					if (this.root.isLeafNode()) {
						LeafNode leafRoot = (LeafNode) this.root;
						leafRoot.deleteAtIndex(leafRoot.search(merge.oldKey));
					}
					else {
						InnerNode innerRoot = (InnerNode) this.root;
						innerRoot.deleteAtIndex(innerRoot.search(merge.oldKey), merge);
						this.root = innerRoot;
					}
				}
				else if (merge.right != null) { // Full level was deleted
					System.out.println("merge.right is not null");
					if (merge.right.isLeafNode()) {
						System.out.println("merge.right is a InnerNode that is now the root");
						LeafNode leafRoot = (LeafNode) merge.right;
						leafRoot.setAsRoot();
						this.root = leafRoot;
					}
					else {
						System.out.println("merge.right is an InnerNode that is now the root");
						this.root = merge.right;
					}
				}
				else if (merge.left != null) {
					System.out.println("merge.left is not null");
					if (merge.left.isLeafNode()) {
						System.out.println("merge.left is a LeafNode that is now the root");
						LeafNode leafRoot = (LeafNode) merge.left;
//						leafRoot.setAsRoot();
						this.root = leafRoot;
					}
					else {
						this.root = merge.left;
					}
				}
				else if (!this.root.isLeafNode()) {
					System.out.println("merge has two children, same root remains");
					InnerNode innerRoot = new InnerNode(this.degree);
					innerRoot.deleteAtIndex(innerRoot.search(merge.oldKey), merge);
					this.root = innerRoot;
				}
			}
			this.root.setAsRoot();
			System.out.println("Dumping Final Tree");
			this.root.dump();
		}
	}
//		Field key = value.getField();
//		if (this.search(key) == null) {
//			System.out.println(" _____ NEW DELETE _____\nkey=" + key);
//			Node balanceNode = null;
//			this.root = findRebalance(key, value, this.root, null, null, null, null, null, balanceNode);
//			
//		}
//	}
	
//	public Node findRebalance(Field key, Entry value, Node curr, Node left, Node right, Node lAnc, Node rAnc, Node parent, Node balanceNode) {
//		 Node newMe = null;
//		 Node nextNode = null;
//		 Node nextLeft = null;
//		 Node nextRight = null;
//		 Node nextAncL = null;
//		 Node nextAncR = null;
//		 
//		 // Mark the nodes that need rebalancing on the recursive way down
//		 if (!curr.testMinimalSize()) {
//			 curr.setMergePath(false);
//		 }
//		 else if (curr.getMergePath() == false) {
//			 curr.setMergePath(true);
//		 }
//		 
//		 if (!curr.isLeafNode()) {
//			 InnerNode currInnerNode = (InnerNode) curr;
//			 nextNode = currInnerNode.getChild(key);
//			 int nextNodeIndex = currInnerNode.search(key);
//			 if (nextNodeIndex == 0) {
//				 nextLeft = (left == null) ? null : ((InnerNode) left).getLastNodePointer();
//				 nextAncL = lAnc;
//			 }
//			 else {
//				 nextLeft = ((InnerNode) left).get
//			 }
//		 }
//		return null;
//	}
	
	public Node getRoot() {
		return this.root;
	}
	
	public void dump() {
		root.dump();
	}
	
	
}

	
//	private int degree;
//	private Node root;
//    
//    public BPlusTree(int degree) {
//    	//your code here
//    	this.degree = degree;
//    	this.root = null;
//    }
//    
//    public LeafNode search(Field f) {
//    	// Assign root node to currentNode variable
//    	Node currentNode = root;
//    	boolean found = false;
//    	while (!currentNode.isLeafNode()) {
//    		// Cast node to inner node so we can grab the keys and children
//    		InnerNode currentInnerNode = (InnerNode) currentNode;
//    		// Grab keys and children
//    		ArrayList<Field> nodeKeys = currentInnerNode.getKeys();
//    		ArrayList<Node> nodeChildren = currentInnerNode.getChildren();
//    		// If the search key is smaller than the smallest inner node key value we can
//    		// set the next node to be the first child and be done with this iteration
//    		if (f.compare(RelationalOperator.LTE, nodeKeys.get(0))) {
//    			currentNode = nodeChildren.get(0);
//    			found = true;
//    			continue;
//    		}
//    		// If the search key is larger than the largest inner node key value we can
//    		// set the next node to be the last child and be done with this iteration
//    		if (f.compare(RelationalOperator.GT, nodeKeys.get(nodeKeys.size()-1))) {
//    			currentNode = nodeChildren.get(nodeChildren.size()-1);
//    			found = true;
//    			continue;
//    		}
//    		// Otherwise check if the value is in between the last key value and the current key value
//    		for (int i = 1; i < nodeKeys.size(); i++) {
//    			if (f.compare(RelationalOperator.GT, nodeKeys.get(i-1)) && f.compare(RelationalOperator.LTE, nodeKeys.get(i))) {
//    				currentNode = nodeChildren.get(i);
//    				found = true;
//    				break;
//    			}
//    		}
//    	}
//    	
////    	LeafNode currentLeafNode = (LeafNode) currentNode;
////    	ArrayList<Entry> nodeEntries = currentLeafNode.getEntries();
////    	for (Entry entry : nodeEntries) {
////    		if (f.compare(RelationalOperator.EQ, entry.getField())) {
////    			System.out.println("Entry Found");
////    			return;
////    		}
////    	}
//    	
//    	if (found) {
//    		// Just have to return the leaf node
//    		return (LeafNode) currentNode;
//    	}
//    	else {
//    		return null;
//    	}
//    	
//    	
//    }
//    
//    /*
//     * public void insert(Field key, Entry value)
//     *   call insert on root node and check result
//     * public void find(Field key)
//     *   start at root, look through node's children until you find a leaf node
//     *   search for entry in leaf node
//     * public void dump() --> prints tree out to screen
//     * 
//     */
//   
//    
//    @SuppressWarnings("unchecked")
//	public void insert(Entry e) {
//    	//your code here
//    	Node currentNode = root;
//    	Stack<Node> parentNodeStack = new Stack<Node>();
//    	
//    	// If the root isnt null then 
//    	
//    	if (currentNode != null) {
//    		// Everything from here to the break was copied from search(Field f), the only difference is it
//        	// adds parent nodes to the stack created above ###############################################
//        	while (!currentNode.isLeafNode()) {
//        		// Add parent node to the stack in case of the need to split
//        		parentNodeStack.push(currentNode);
//        		// Cast node to inner node so we can grab the keys and children
//        		InnerNode currentInnerNode = (InnerNode) currentNode;
//        		// Grab keys and children
//        		ArrayList<Field> nodeKeys = currentInnerNode.getKeys();
//        		ArrayList<Node> nodeChildren = currentInnerNode.getChildren();
//        		// If the search key is smaller than the smallest inner node key value we can
//        		// set the next node to be the first child and be done with this iteration
//        		if (e.getField().compare(RelationalOperator.LTE, nodeKeys.get(0))) {
//        			currentNode = nodeChildren.get(0);
//        			continue;
//        		}
//        		// If the search key is larger than the largest inner node key value we can
//        		// set the next node to be the last child and be done with this iteration
//        		if (e.getField().compare(RelationalOperator.GT, nodeKeys.get(nodeKeys.size()-1))) {
//        			currentNode = nodeChildren.get(nodeChildren.size()-1);
//        			continue;
//        		}
//        		// Otherwise check if the value is in between the last key value and the current key value
//        		for (int i = 1; i < nodeKeys.size(); i++) {
//        			if (e.getField().compare(RelationalOperator.GT, nodeKeys.get(i-1)) && e.getField().compare(RelationalOperator.LTE, nodeKeys.get(i))) {
//        				currentNode = nodeChildren.get(i);
//        				break;
//        			}
//        		}
//        	}
//        	// ############################################################################################
//    	}
//    	else {
//    		//the tree should have only a single leaf node when there is initially no root. 
//    		this.root = new LeafNode(degree);
//			LeafNode firstLeafNode = new LeafNode(degree);
//			firstLeafNode.addEntry(e, false);
//			//this.root.addKeyAndChild(firstLeafNode.getLargestKeyValue(), firstLeafNode, false);
//			this.root = firstLeafNode;
//			return; // If this is the first node then return here
//    	}
//    	
//    	LeafNode currentLeafNode = (LeafNode) currentNode;
//    	ArrayList<Entry> nodeEntries = currentLeafNode.getEntries();
//    	for (Entry entry : nodeEntries) {
//    		if (e.getField().compare(RelationalOperator.EQ, entry.getField())) {
//    			System.out.println("Entry Already in File");
//    			return;
//    		}
//    	}
//    	if (!currentLeafNode.willBeOverCapacity()) {
//    		currentLeafNode.addEntry(e, true);
//    		return; // We successfully added the entry and we're done
//    	}
//    	else {
//    		LeafNode tmpLeafNode = new LeafNode(degree+1);
//    		tmpLeafNode.addMultipleEntries(currentLeafNode.getEntries(), false);
//    		tmpLeafNode.addEntry(e, true); 									 
//    		ArrayList<Entry> tmpLeafNodeEntries = tmpLeafNode.getEntries();
//    		LeafNode splitLeafNode = new LeafNode(degree);
//    		ArrayList<Entry> splitNodeHolderArray = new ArrayList<Entry>();
//    		ArrayList<Entry> existingNodeHolderArray = new ArrayList<Entry>();
//    		
//    		for(int i = 0; i < tmpLeafNodeEntries.size(); i++) {
//    			if (i <= Math.ceil(tmpLeafNodeEntries.size()/2)) {
//    				existingNodeHolderArray.add(e);
//    			}
//    			else {
//    				splitNodeHolderArray.add(e);
//    			}
//    		}
//    		splitLeafNode.addMultipleEntries(splitNodeHolderArray, false); // Should already be in sorted order
//    		
//    		currentLeafNode.emptyNode();
//    		currentLeafNode.addMultipleEntries(existingNodeHolderArray, false); // Here too
//    		
//    		boolean finished = false;
//    		
//    		while(!finished) {
//    			if (parentNodeStack.isEmpty()) {
//    				this.root = new InnerNode(degree);
//    				LeafNode firstLeafNode = new LeafNode(degree);
//    				firstLeafNode.addEntry(e, false);
//    				this.root.addKeyAndChild(firstLeafNode.getLargestKeyValue(), firstLeafNode, false);
//    				finished = true;
//    			}
//    			else {
//    				InnerNode currentParentNode = (InnerNode) parentNodeStack.pop(); // We know parent nodes will be InnerNodes
//    				if (!currentParentNode.willBeOverCapacity()) {
//    					currentParentNode.addKeyAndChild(splitLeafNode.getLargestKeyValue(), splitLeafNode, true);
//    				}
//    				else {
//    					//copy n to temp
//    					InnerNode tmpInnerNode = new InnerNode(degree+1);
//    					tmpInnerNode.addMultipleKeys(currentParentNode.getKeys(), currentParentNode.getKeyNodeMap(), false);
//    					tmpInnerNode.addKeyAndChild(splitLeafNode.getLargestKeyValue(), splitLeafNode, true);
//    					InnerNode splitInternalNode = new InnerNode(degree);
//    					HashMap<Field,Node> splitInternalNodeMap = new HashMap<Field,Node>();
//    					HashMap<Field,Node> existingInternalNodeMap = new HashMap<Field,Node>();
//    					for (int i = 0; i < tmpInnerNode.getKeys().size(); i++) {
//    						if (i < Math.floor(tmpInnerNode.getKeys().size()/2)) {
//    							splitInternalNodeMap.put(tmpInnerNode.getKeys().get(i), tmpInnerNode.getKeyNodeMap().get(tmpInnerNode.getKeys().get(i)));
//    						}
//    						else {
//    							existingInternalNodeMap.put(tmpInnerNode.getKeys().get(i), tmpInnerNode.getKeyNodeMap().get(tmpInnerNode.getKeys().get(i)));
//    						}
//    					}
//    					splitInternalNode.addMultipleKeys((ArrayList<Field>) splitInternalNodeMap.keySet(), splitInternalNodeMap, false); // Field Values should be sorted already
//    					
//    					currentParentNode.emptyNode();
//    					currentParentNode.addMultipleKeys((ArrayList<Field>) splitInternalNodeMap.keySet(), splitInternalNodeMap, false); // Here too
//    					
//    					// How do I account for pushing through?
//    					if (!parentNodeStack.isEmpty()) {
//    						InnerNode grandparentNode = (InnerNode) parentNodeStack.pop();
//    						grandparentNode.addKeyAndChild(splitInternalNode.getLargestKeyValue(), splitInternalNode, true);
//    						parentNodeStack.push(grandparentNode);
//    					}
//    				}
//    			}
//    		}
//    	}
//    }
//    
//    public void delete(Entry e) {
//    	//your code here
//    	InnerNode balanceNode = null;
//    	this.root = findRebalance(this.root,null,null,null,null,e,balanceNode);
//    }
//    
//    public InnerNode findRebalance(Node thisNode, Node leftNode, Node rightNode, InnerNode lAnchor, 
//    		InnerNode rAnchor, Entry key, InnerNode balanceNode) {
//    	Node removeNode = null;
//    	Node nextNode = null;
//    	Node nextLeft = null;
//    	Node nextRight = null;
//    	InnerNode nextAncL = null;
//    	InnerNode nextAncR = null;
//    	int indexOfEntryInNode = -1;
//    	
//    	// Find the nodes that need rebalancing
//    	if (!thisNode.isUnderCapacity()) {
//    		balanceNode = null; // No balance
//    	}
//    	else if (balanceNode == null) {
//    		balanceNode = (InnerNode) thisNode;
//    	}
//    	
//    	// Calculate Neighbor and Anchor nodes
//    	if (!thisNode.isLeafNode()) {
//    		InnerNode thisInnerNode = (InnerNode) thisNode;
//    		ArrayList<Field> thisInnerNodeKeys = thisInnerNode.getKeys();
//    		Pair<Field,Node> nextKeyAndNodePair = thisInnerNode.findNextNodeForKey(key);
//    		Field nextKey = nextKeyAndNodePair.getLeft();
//    		nextNode = nextKeyAndNodePair.getRight();
//    		if (nextKeyAndNodePair.getLeft().compare(RelationalOperator.LT, thisInnerNodeKeys.get(0))) {
//    			Field nextLeftKey = leftNode.getLargestKeyValue();
//    			nextLeft = thisInnerNode.getChildForKey(nextLeftKey);
//    			nextAncL = lAnchor;
//    		}
//    		else {
//    			Field nextLeftKey = leftNode.getPreviousPointer(key);
//    			nextLeft = thisInnerNode.getChildForKey(nextLeftKey);
//    			nextAncL = thisInnerNode;
//    		}
//    		if (nextKeyAndNodePair.getLeft().compare(RelationalOperator.GT, thisInnerNodeKeys.get(thisInnerNodeKeys.size()-1))) {
//    			Field nextRightKey = rightNode.getSmallestKeyValue();
//    			nextRight = thisInnerNode.getChildForKey(nextRightKey);
//    			nextAncR = rAnchor;
//    		}
//    		else {
//    			Field nextRightKey = rightNode.getNextPointer(key);
//    			nextRight = thisInnerNode.getChildForKey(nextRightKey);
//    			nextAncR = thisInnerNode;
//    		}
//    		
//    		removeNode = findRebalance(nextNode,nextLeft, nextRight, nextAncL, nextAncR, key, balanceNode);
//    	}
//    	else { // Key was found or not
//    		LeafNode thisLeafNode = (LeafNode) thisNode;
//    		indexOfEntryInNode = thisLeafNode.findIndex(key);
//    		if (indexOfEntryInNode != -1) {
//    			removeNode = thisLeafNode; // removing entry from this leaf Node
//    		}
//    		else {
//    			removeNode = null;
//    		}
//    	}
//    	
//    	if (removeNode.equals(nextNode)) {
//    		removeNode.deleteEntry(indexOfEntryInNode);
//    	}
//    	Node done = null;
//    	if (balanceNode == null) {
//    		done = null;
//    	}
//    	else if (thisNode.equals(this.root)) {
//    		done = collapseRoot(thisNode);
//    	}
//    	return null;
//    }
//    
//    public Node collapseRoot(Node oldRoot) {
//    	Node newRoot = null;
//    	if (oldRoot.isLeafNode()) {
//    		newRoot = null;
//    	}
//    	else {
//    		InnerNode oldInnerRoot = (InnerNode) oldRoot;
//    		Field soleRootChildPointerKeyValue = oldRoot.getSmallestKeyValue();
//    		newRoot = oldInnerRoot.getChildForKey(soleRootChildPointerKeyValue);
//    	}
//    	
//    	return newRoot;
//    }
//    
//    public Node rebalance(Node thisNode, Node leftNode, Node rightNode, InnerNode lAnchor) {
//    	// The balance node will be set to the more full of the two neighbors
//    	Node balanceNode = (leftNode.capacity() <= rightNode.capacity()) ? rightNode : leftNode;
//    	if (balanceNode.is)
//    	return null;
//    }
//    
//    public Node getRoot() {
//    	return this.root;
//    }
//    
//
//
//	
//}
