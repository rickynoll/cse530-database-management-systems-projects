package hw3;

import java.util.ArrayList;

import hw1.Field;
import hw1.RelationalOperator;

public class LeafNode implements Node {
	private int degree;
	private InnerNode parent;
	private ArrayList<Field> keys;
	private ArrayList<Entry> entries;
	private LeafNode nextNode;
	private boolean foundRightSibling;
	private boolean isRoot;
	
	// Construct a new empty leaf Node
	public LeafNode(int degree) {
		this.degree = degree;
		this.parent = null;
		this.keys = new ArrayList<Field>();
		this.entries = new ArrayList<Entry>();
		this.nextNode = null;
		this.foundRightSibling = false;
		this.isRoot = false;
	}
	
	// Construct a new empty leaf Node that is the root
	public LeafNode(int degree, boolean isRoot) {
		this.degree = degree;
		this.parent = null;
		this.keys = new ArrayList<Field>();
		this.entries = new ArrayList<Entry>();
		this.nextNode = null;
		this.foundRightSibling = false;
		this.isRoot = isRoot;
	}
	
//	public LeafNode(int degree, Field parent)
	
	public int search(Field key) {
		for (int i = 0; i < this.keys.size(); i++) {
			if (this.keys.get(i).compare(RelationalOperator.EQ, key)) {
				return i;
			}
		}
		return -1;
	}
	
	public Split insert(Field key, Entry value) {
		this.insertEntry(key, value);
		if (this.keys.size() > degree) {
			// Create a Sibling for the split
			LeafNode sibling = new LeafNode(degree);
			sibling.keys = new ArrayList<Field>(this.keys.subList((int) Math.ceil((degree+1)/2.0), degree+1));
			sibling.entries = new ArrayList<Entry>(this.entries.subList((int) Math.ceil((degree+1)/2.0), degree+1));
			this.keys.subList((int) Math.ceil((degree+1)/2.0), degree+1).clear();
			this.entries.subList((int) Math.ceil((degree+1)/2.0), degree+1).clear();
			sibling.nextNode = this.nextNode;
			this.nextNode = sibling;
			this.isRoot = false; // if this node was the root node, let it know it isn't anymore
			System.out.println("Original leaf node's new keys -> " + this.keys);
			System.out.println("New sibling's new keys -> " + sibling.keys);
			Split split = new Split(this.keys.get(this.keys.size()-1), this, sibling);
			System.out.println("Split object being returned . . .");
			split.dump();
			return split;
		}
		return null;
	}
	
	public void insertEntry(Field key, Entry value) {
		if (this.keys.isEmpty()) {
			System.out.println("thisLeafNode.keys -> " + this.keys);
			this.addKey(key);
			this.addEntry(value);
			System.out.println("New Entry inserted into LeafNode at pos 0 because of empty key list");
			System.out.println("* NEW * thisLeafNode.keys -> " + this.keys);
			return;
		}
		System.out.println("thisLeafNode.keys -> " + this.keys);
		System.out.println("Leaf Node key being compared: " + key + " <= " + this.keys.get(0));
		if (key.compare(RelationalOperator.LTE, this.keys.get(0))) {
			this.addKey(0, key);
			this.addEntry(0, value);
			System.out.println("New Entry inserted into LeafNode at pos 0 because of smaller key");
			System.out.println("* NEW * thisLeafNode.keys -> " + this.keys);
			return;
		}
		System.out.println("Leaf Node key being compared: " + key + " > " + this.keys.get(this.keys.size()-1));
		if (key.compare(RelationalOperator.GT, this.keys.get(this.keys.size()-1))) {
			this.addKey(key);
			this.addEntry(value);
			System.out.println("New Entry inserted into LeafNode at pos " + (this.keys.size()-1) + " (end of list)");
			System.out.println("* NEW * thisLeafNode.keys -> " + this.keys);
			return;
		}
		for (int i = 1; i < this.entries.size(); i++) {
			System.out.println("Leaf Node key being compared: " + this.keys.get(i-1) + " < " + key + " <= " + this.keys.get(i));
			if (key.compare(RelationalOperator.GT, this.keys.get(i-1)) && key.compare(RelationalOperator.LTE, this.keys.get(i))) {
				this.addKey(i, key);
				this.addEntry(i, value);
				System.out.println("New Entry inserted into LeafNode at pos " + i + " (middle of list)");
				System.out.println("* NEW * thisLeafNode.keys -> " + this.keys);
				return;
			}
		}
	}
	
	public Merge delete(Field key) {
		int indexToDelete = this.search(key);
		if (indexToDelete != -1) {
			Field oldKey = this.getParentKey();
			this.deleteAtIndex(indexToDelete);
			if (!this.isRoot) {
				LeafNode sibling = (LeafNode) this.getNearestSibling(key);
				Merge merge = null;
				Field newKey = null;
				if (this.checkUnderflow()) {
					System.out.println("Sibling capacity check: " + sibling.capacity() + " >= " + ((int)Math.ceil((degree+1)/2.0)));
					if (sibling.capacity() >= Math.ceil((degree+1)/2.0)) { // Entry can be taken from sibling
						System.out.println("Taking entry from sibling");
						if (this.foundRightSibling) { // Taking Entry from right sibling
							newKey = sibling.transferEntryLeft(this);
							merge = new Merge(newKey, oldKey, this, sibling);
						}
						else { // Taking Entry from left sibling
							newKey = sibling.transferEntryRight(this);
							merge = new Merge(newKey, sibling.getParentKey(), sibling, this);
						}
					}
					else { // Nodes have to be merged
						System.out.println(" \n$ $ $ Merging Leaf Nodes $ $ $");
						LeafNode newMergedNode = (LeafNode) this.mergeWith(sibling, key);
						if (this.foundRightSibling) {
							System.out.println();
							newKey = newMergedNode.getParentKey();
							merge = new Merge(newKey, oldKey, newMergedNode, null); // Case 2: Under capacity node was right node
						}
						else {
							newKey = newMergedNode.getParentKey();
							merge = new Merge(newKey, sibling.getParentKey(), null, newMergedNode); // Case 3: Under capacity node was left node
						}
					}
				}
				else if (indexToDelete == this.keys.size()) {
					newKey = this.getParentKey();
					System.out.println("I'm here!");
					int indexToReplace = this.getIndexInParent(newKey);
					this.parent.setKey(indexToReplace, newKey);
					merge = null;
				}
				System.out.println("Merge object being returned . . .");
				//merge.dump();
				return merge;
			}
			else {
				System.out.println("This node is the root node :: no capacity check done.");
				return null;
			}
		}
		else {
			System.out.println("Entry not in Leaf Node!");
			return null;
		}
	}
	
	public boolean checkUnderflow() {
		if (this.degree % 2 == 0) {
			System.out.println("Underflow check: " + this.keys.size() + " < " + ((int)Math.ceil((degree+1)/2.0)-1));
			return this.keys.size() < Math.ceil((degree+1)/2.0)-1;
		}
		else {
			System.out.println("Underflow check: " + this.keys.size() + " <= " + ((int)Math.ceil((degree+1)/2.0)-1));
			return this.keys.size() <= Math.ceil((degree+1)/2.0)-1;
		}
	}
	
	public void deleteAtIndex(int index) {
		System.out.println("\nDeleted key=" + this.keys.remove(index) + " from LeafNode");
		this.entries.remove(index);
	}
	
	public Node getNearestSibling(Field key) {
		int thisIndexInParent = this.getIndexInParent(key);
		System.out.println("\nthisIndexInParent=" + thisIndexInParent);
		if (thisIndexInParent <= 0) {
			this.foundRightSibling = true;
			System.out.println("Right sibling found -- only sibling available\n");
			return this.parent.getNextPointer(thisIndexInParent); // Right Sibling
		}
		else if (thisIndexInParent >= degree) {
			this.foundRightSibling = false;
			System.out.println("Left sibling found -- only sibling available\n");
			return this.parent.getPreviousPointer(thisIndexInParent); // Left Sibling
		}
		else if (thisIndexInParent != -1){
			Node leftSibling = this.parent.getPreviousPointer(thisIndexInParent); // Left Sibling
			Node rightSibling = this.parent.getNextPointer(thisIndexInParent); // Right Sibling
			if (rightSibling.capacity() >= leftSibling.capacity()) {
				this.foundRightSibling = true;
				System.out.println("Right sibling found -- had more entries than left sibling\n");
				return rightSibling;
			}
			else {
				this.foundRightSibling = false;
				System.out.println("Left sibling found -- had more entries than right sibling\n");
				return leftSibling;
			}
		}
		System.out.println("ERROR: Could not find index in parent node.");
		return null;
	}
	
	public Field transferEntryLeft(LeafNode sibling) {
		Field newKey = null;
		newKey = this.keys.get(0);
		System.out.println("Node being transfered from - - -");
		this.dump();
		System.out.println("Node being transfered to   + + +");
		sibling.dump();
		int thisIndexInParent = this.getIndexInParent(newKey);
		Field keyInParent = this.parent.getKey(thisIndexInParent);
		System.out.println("keyInParent=" + keyInParent);
		if (thisIndexInParent == 0) {
			System.out.println("Key in parent being replaced key=" + keyInParent + " with newKey=" + newKey);
			this.parent.setKey(thisIndexInParent, newKey);
		}
		else {
			System.out.println("Key in parent being replaced key=" + keyInParent + " with newKey=" + newKey);
			this.parent.setKey(thisIndexInParent-1, newKey);
		}
		sibling.addKey(keyInParent);
		sibling.addEntry(this.entries.get(0));
		this.keys.remove(keyInParent);
		this.entries.remove(this.entries.get(0));
		System.out.println("\nShifted key=" + keyInParent + " to left node");
		return newKey;
	}
	
	public Field transferEntryRight(LeafNode sibling) {
		Field newKey = null;
		newKey = this.keys.get(this.keys.size()-2);
		System.out.println("Node being transfered from - - -");
		this.dump();
		System.out.println("Node being transfered to   + + +");
		sibling.dump();
		int thisIndexInParent = this.getIndexInParent(newKey);
		Field keyInParent = this.parent.getKey(thisIndexInParent);
		if (thisIndexInParent == 0) {
			System.out.println("Key in parent being replaced key=" + keyInParent + " with newKey=" + newKey);
			this.parent.replaceKey(thisIndexInParent, newKey);
		}
		else {
			System.out.println("Key in parent being replaced key=" + keyInParent + " with newKey=" + newKey);
			this.parent.replaceKey(thisIndexInParent-1, newKey);
		}
		sibling.addKey(0, keyInParent);
		sibling.addEntry(0, this.entries.get(this.entries.size()-1));
		this.keys.remove(keyInParent);
		this.entries.remove(this.entries.get(this.entries.size()-1));
		System.out.println("Shifted key=" + keyInParent + " to right node");
		return newKey;
	}
	
	public Node mergeWith(Node sibling, Field oldKey) {
		int indexInParent = this.getIndexInParent(oldKey);
		System.out.println("This indexInParent=" + indexInParent);
		LeafNode leafSibling = (LeafNode) sibling;
		LeafNode newMergedNode = new LeafNode(this.degree);
		ArrayList<Field> newMergedKeys = new ArrayList<Field>();
		ArrayList<Entry> newMergedEntries = new ArrayList<Entry>();
		if (this.foundRightSibling) {
			newMergedKeys.addAll(this.keys);
			this.keys.addAll(leafSibling.keys);
			System.out.println("Merging this.keys=" + this.keys + " and sibling.keys=" + leafSibling.keys);
			newMergedKeys.addAll(leafSibling.keys);
			newMergedEntries.addAll(this.entries);
			newMergedEntries.addAll(leafSibling.entries);
		}
		else {
			leafSibling.keys.addAll(this.keys);
			System.out.println("Merging this.keys=" + this.keys + " and sibling.keys=" + leafSibling.keys);
			newMergedKeys.addAll(leafSibling.keys);
			newMergedKeys.addAll(this.keys);
			newMergedEntries.addAll(leafSibling.entries);
			newMergedEntries.addAll(this.entries);
		}
		this.parent.removeKeyAndChild(indexInParent);
		newMergedNode.setKeys(newMergedKeys);
		newMergedNode.setEntries(newMergedEntries);
		System.out.println("newMergedNode keys=" + newMergedNode.keys);
		return newMergedNode;
	}
	
	public int getIndexInParent(Field key) {
		return this.parent.search(key);
	}
	
	public void dump() {
	    System.out.println("LeafNode h==0");
	    for (int i = 0; i < this.keys.size(); i++){
	    	System.out.println(this.keys.get(i));
	    }
	}
	
	public int capacity() {
		return this.keys.size();
	}
	
	public boolean isLeafNode() {
		return true;
	}
	
	public void setAsRoot() {
		this.isRoot = true;
	}
	
	public void setParent(Node node) {
		this.parent = (InnerNode) node;
	}
	
	public void setKeys(ArrayList<Field> newKeys) {
		this.keys = newKeys;
	}
	
	public void setEntries(ArrayList<Entry> newEntries) {
		this.entries = newEntries;
	}
	
	public void addKey(Field key) {
		this.keys.add(key);
	}
	
	public void addKey(int index, Field key) {
		this.keys.add(index, key);
	}
	
	public Field getParentKey() {
		return this.keys.get(this.keys.size()-1);
	}
	
	public void addEntry(Entry value) {
		this.entries.add(value);
	}
	
	public void addEntry(int index, Entry value) {
		this.entries.add(index, value);
	}
	
	public ArrayList<Entry> getEntries() {
		return this.entries;
	}

	public String toString() {
		return this.keys.toString();
	}

	@Override
	public boolean testMinimalSize() {
		return this.keys.size() <= Math.ceil((degree+1)/2.0)-1;
	}

	@Override
	public ArrayList<Field> getKeys() {
		return this.keys;
	}
	
}

//	private int degree;
//	private ArrayList<Entry> entries;
//	public LeafNode(int degree) {
//		//your code here
//		this.degree = degree;
//		this.entries = new ArrayList<Entry>();
//	}
//	
//	public LeafNode(LeafNode ln) {
//		//your code here
//		this.degree = ln.degree;
//		this.entries = new ArrayList<Entry>(ln.entries);
//	}
//	
//	public LeafNode(LeafNode ln, boolean deepcopy) {
//		//your code here
//		this.degree = ln.degree;
//		this.entries = new ArrayList<Entry>(ln.entries);
//	}
//	
//	/*
//	 * Source : http://jxlilin.blogspot.com/2013/11/b-tree-implementation-in-java.html
//	 * getValue(int index)
//	 * setValue(int index, Entry e)
//	 * isLeafNode()
//	 * search(Entry e)
//	 * insertAt(int index, Entry e)
//	 * delete(Entry e)
//	 * deleteAt(Entry e)
//	 */
//	
//	public void addEntry(Entry e, boolean willSort) {
//		entries.add(e);
//		if (willSort) {
//			sortEntries();
//		}
//		return;
//	}
//	
//	public void addMultipleEntries(ArrayList<Entry> entryArrayList, boolean willSort) {
//		this.entries = new ArrayList<Entry>(entryArrayList);
//		if (willSort) {
//			sortEntries();
//		}
//		return;
//	}
//	
//	public boolean isUnderCapacity() {
//		return (entries.size() < degree/2) ? true : false;
//	}
//	
//	public boolean isOverCapacity() {
//		return (entries.size() > degree) ? true : false;
//	}
//	
//	public boolean willBeUnderCapacity() {
//		return (entries.size()-1 < degree/2) ? true : false;
//	}
//	
//	public boolean willBeOverCapacity() {
//		return (entries.size()+1 > degree) ? true : false;
//	}
//	
//	public void emptyNode() {
//		this.entries.clear();
//		return;
//	}
//	
//	public Field getSmallestKeyValue() {
//		return this.entries.get(0).getField();
//	}
//	
//	public Field getLargestKeyValue() {
//		return this.entries.get(this.entries.size()-1).getField();
//	}
//	
//	public Field getPreviousPointer(Entry key) {
//		int index = this.entries.indexOf(key);
//		return this.entries.get(index-1).getField();
//	}
//	
//	public Field getPreviousPointer(Field key) {
//		return null;
//	}
//	
//	public Field getNextPointer(Entry key) {
//		int index = this.entries.indexOf(key.getField());
//		return this.entries.get(index+1).getField();
//	}
//	
//	public Field getNextPointer(Field key) {
//		return null;
//	}
//	
//	public int findIndex(Entry e) {
//		int index = 0;
//		for (Entry entry : this.entries) {
//			if (entry.compareTo(e) == 0) {
//				return index;
//			}
//			index++;
//		}
//		return -1;
//	}
//	
//	public ArrayList<Entry> getEntries() {
//		//your code here
//		return this.entries;
//	}
//	
//	public void sortEntries() {
//		Collections.sort(this.entries, new Comparator<Entry>(){
//			public int compare(Entry e1,Entry e2){
//				if (e1.getField().compare(RelationalOperator.LT, e2.getField())) {
//					return -1;
//				} else if (e1.getField().compare(RelationalOperator.EQ, e2.getField())){
//					return 0;
//				} else {
//					return 1;
//				}
//          }});
//	}
//
//	
//	public int getDegree() {
//		//your code here
//		return this.degree;
//	}
//	
//	public boolean isLeafNode() {
//		return true;
//	}
//	
//	public void deleteEntry(int i) {
//		this.entries.remove(i);
//	}
//	
//	public void deleteEntry(Entry e) {
//		this.entries.remove(e);
//	}
//	
//	public int capacity() {
//		return this.entries.size();
//	}
//
//	@Override
//	public boolean equals(Node other) {
//		LeafNode otherLeaf = (LeafNode) other;
//		if (this.degree != otherLeaf.getDegree()) {
//			return false;
//		}
//		for (int i = 0; i < this.entries.size(); i++) {
//			if (this.entries.get(i).compareTo(otherLeaf.getEntries().get(i)) != 0) {
//				return false;
//			}
//		}
//		return true;
//	}
//	
//	public void addKeyAndChild(Field f, Node node, boolean willSort) {
//		Entry E = new Entry(f,0);
//		if (this.entries.isEmpty()) {
//			this.entries.add(E);
//			return;
//		}
//		if (f.compare(RelationalOperator.LTE, entries.get(0).getField())) {
//			this.entries.add(E);
//			return;
//		}
//		// If the search key is larger than the largest inner node key value we can
//		// set the next node to be the last child and be done with this iteration
//		if (f.compare(RelationalOperator.GT, entries.get(entries.size()-1).getField())) {
//			this.entries.add(new Entry(f, entries.size()-1));
//			return;
//		}
//		// Otherwise check if the value is in between the last key value and the current key value
//		for (int i = 1; i < entries.size(); i++) {
//			if (f.compare(RelationalOperator.GT, entries.get(i-1).getField()) && f.compare(RelationalOperator.LTE, entries.get(i).getField())) {
//				this.entries.add(new Entry(f, i));
//				return;
//			}
//		}
//		if (willSort) {
//			sortEntries();
//		}
//		return;
//
//	}
//
//}