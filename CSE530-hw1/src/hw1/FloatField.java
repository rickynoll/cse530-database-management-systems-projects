package hw1;

import java.io.*;
import java.nio.ByteBuffer;

/**
 * Instance of Field that stores a single integer.
 */
public class FloatField implements Field {
    private float value;

    public float getValue() {
        return value;
    }

    /**
     * Constructor.
     *
     * @param i The value of this field.
     */
    public FloatField(float i) {
        value = i;
    }
    
    public FloatField(byte[] b) {
    	value = java.nio.ByteBuffer.wrap(b).getFloat();
    }

    public String toString() {
        return Float.toString(value);
    }

    public int hashCode() {
    	Float fobj = new Float(value);
        return fobj.hashCode();
    }

    public boolean equals(Object field) {
        return ((FloatField) field).value == value;
    }

    public void serialize(DataOutputStream dos) throws IOException {
        dos.writeFloat(value);
    }
    
    public byte[] toByteArray() {
    	return ByteBuffer.allocate(4).putFloat(value).array();
    }

    /**
     * Compare the specified field to the value of this Field.
     * Return semantics are as specified by Field.compare
     *
     * @throws IllegalCastException if val is not an IntField
     * @see Field#compare
     */
    public boolean compare(RelationalOperator op, Field val) {

        FloatField fVal = (FloatField) val;

        switch (op) {
        case EQ:
            return value == fVal.value;
        case NOTEQ:
            return value != fVal.value;
        case GT:
            return value > fVal.value;
        case GTE:
            return value >= fVal.value;
        case LT:
            return value < fVal.value;
        case LTE:
            return value <= fVal.value;
        }
        return false;
    }

    /**
     * Return the Type of this field.
     * @return Type.INT_TYPE
     */
	public Type getType() {
		return Type.FLOAT;
	}
}
