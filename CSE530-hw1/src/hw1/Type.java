package hw1;

public enum Type {
	INT,
	STRING,
	BOOLEAN,
	FLOAT,
	DATE,
	TIME,
	DATETIME
}
