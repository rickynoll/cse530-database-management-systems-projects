package hw1;

import java.nio.ByteBuffer;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

/**
 * This class represents a tuple that will contain a single row's worth of information
 * from a table. It also includes information about where it is stored
 * @author Sam Madden modified by Doug Shook
 *
 */
public class Tuple {
	
	private HashMap<String,byte[]> tuple;
//	private HashMap<String,Field> tuple;
	private TupleDesc td;
	private int pageId;
	private int tupleId;
	
	/**
	 * Creates a new tuple with the given description
	 * @param t the schema for this tuple
	 */
	public Tuple(TupleDesc t) {
		this.td = t;
//		this.tuple = new HashMap<String,Field>();
		this.tuple = new HashMap<String,byte[]>();
		this.pageId = 0;
		this.tupleId = 0;
	}
	
	public Tuple(Tuple s){
		this.td = s.getDesc();
		this.tuple = new HashMap<String,byte[]>();
		this.pageId = s.getPid();
		this.tupleId = s.getId();
	}
	
	public TupleDesc getDesc() {
		return td;
	}
	
	/**
	 * retrieves the page id where this tuple is stored
	 * @return the page id of this tuple
	 */
	public int getPid() {
		return pageId;
	}

	public void setPid(int pid) {
		this.pageId = pid;
	}

	/**
	 * retrieves the tuple (slot) id of this tuple
	 * @return the slot where this tuple is stored
	 */
	public int getId() {
		return tupleId;
	}

	public void setId(int id) {
		this.tupleId = id;
	}
	
	public void setDesc(TupleDesc td) {
		this.td = td;
	}
	
	/**
	 * Stores the given data at the i-th field
	 * @param i the field number to store the data
	 * @param v the data
	 */
	public void setField(int i, byte[] v) {
		String key = this.td.getFieldName(i);
		this.tuple.put(key, v);
	}
	
	public byte[] getField(int i) {
		String key = td.getFieldName(i);
		return tuple.get(key);
	}
	
	public Type getType(String s) {
		return this.td.getType(s);
	}
	
	public boolean equals(Object o) {
		Tuple other = (Tuple) o;
		Set<String> keys = tuple.keySet();
		for (String key : keys) {
			switch(getType(key)) {
			case INT:
				return new IntField(this.tuple.get(key)).equals(new IntField(other.tuple.get(key)));
			case STRING:
				return new StringField(this.tuple.get(key)).equals(new StringField(other.tuple.get(key)));
			case BOOLEAN:
				return new BooleanField(this.tuple.get(key)).equals(new BooleanField(other.tuple.get(key)));
			case FLOAT:
				return new FloatField(this.tuple.get(key)).equals(new FloatField(other.tuple.get(key)));
			case DATE:
				return new DateField(this.tuple.get(key)).equals(new DateField(other.tuple.get(key)));
			case TIME:
				return new TimeField(this.tuple.get(key)).equals(new TimeField(other.tuple.get(key)));
			case DATETIME:
				return new DateTimeField(this.tuple.get(key)).equals(new DateTimeField(other.tuple.get(key)));
			}
			
		
		}
		return true;
	}
	
	/**
	 * Creates a string representation of this tuple that displays its contents.
	 * You should convert the binary data into a readable format (i.e. display the ints in base-10 and convert
	 * the String columns to readable text).
	 */
	public String toString() {
		Set<String> keys = tuple.keySet();
		String returnString = "( ";
		Iterator<String> keyIterator = keys.iterator();
		int count = 0;
		while (keyIterator.hasNext()) {
			if (this.td.getType(count) == Type.INT) {
				returnString += ByteBuffer.wrap(tuple.get(keyIterator.next())).getInt();
				returnString += " ";
			}
			else if (this.td.getType(count) == Type.STRING) {
				returnString += new String(tuple.get(keyIterator.next()));
				returnString += " ";
			}
			else if (this.td.getType(count) == Type.BOOLEAN) {
				returnString += (ByteBuffer.wrap(tuple.get(keyIterator.next())).get() == (byte) 0) ? "false" : "true";
				returnString += " ";
			}
			else if (this.td.getType(count) == Type.FLOAT) {
				returnString += ByteBuffer.wrap(tuple.get(keyIterator.next())).getFloat();
				returnString += " ";
			}
			else if (this.td.getType(count) == Type.DATE) {
				long epochSeconds = ByteBuffer.wrap(tuple.get(keyIterator.next())).getLong();
				returnString += new DateField(epochSeconds).toString();
				returnString += " ";
			}
			else if (this.td.getType(count) == Type.TIME) {
				long epochNanoseconds = ByteBuffer.wrap(tuple.get(keyIterator.next())).getLong();
				returnString += new TimeField(epochNanoseconds).toString();
				returnString += " ";
			}
			else if (this.td.getType(count) == Type.DATETIME) {
				long epochSeconds = ByteBuffer.wrap(tuple.get(keyIterator.next())).getLong();
				returnString += new DateTimeField(epochSeconds).toString();
				returnString += " ";
			}
			else {
				returnString += " _____ ";
			}
			count++;
		}
		returnString += " )";
		return returnString;
	}
}
	