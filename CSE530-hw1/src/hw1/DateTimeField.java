package hw1;

import java.io.*;
import java.nio.ByteBuffer;
import java.time.*;

/**
 * Instance of Field that stores a single integer.
 */
public class DateTimeField implements Field {
	
	private LocalDateTime dateTime;

    public LocalDateTime getValue() {
        return dateTime;
    }

    /**
     * Constructor.
     * 
     * *Note:* Using deprecated Date(int,int,int) constructors because it probably won't
     *	be unsupported by the time I turn this in.
     *
     * Includes primitive bounds checking for values,
	 * does not handle specific 28/30/31 day differences in months
	 * 
	 * @param year value between 0-9999 representing the year
	 * @param month value between 1-12 representing the month
	 * @param day value between 1-31 representing the day
	 * 
	 */
	public DateTimeField(int year, int month, int day, int hour, int min) {
        this.dateTime = LocalDateTime.of(year, month, day, hour, min);
    }
	
	public DateTimeField(int year, int month, int day, int hour, int min, int sec) {
        this.dateTime = LocalDateTime.of(year, month, day, hour, min, sec);
    }
	
	public DateTimeField(int year, int month, int day, int hour, int min, int sec, int nano) {
        this.dateTime = LocalDateTime.of(year, month, day, hour, min, sec, nano);
    }
	
	public DateTimeField(long epochSecond) {
        this.dateTime = LocalDateTime.ofEpochSecond(epochSecond, 0, ZoneOffset.UTC);
    }
	
	public DateTimeField(long epochSecond, ZoneOffset zoneId) {
        this.dateTime = LocalDateTime.ofEpochSecond(epochSecond, 0, zoneId);
    }
	
	public DateTimeField(long epochSecond, int nanoSeconds, ZoneOffset zoneId) {
        this.dateTime = LocalDateTime.ofEpochSecond(epochSecond, nanoSeconds, zoneId);
    }
    
    public DateTimeField(byte[] b) {
    	this.dateTime = LocalDateTime.ofInstant(Instant.ofEpochSecond(java.nio.ByteBuffer.wrap(b).getLong()), ZoneOffset.UTC);
    }

    public String toString() {
        return dateTime.toString();
    }

    public int hashCode() {
    	return dateTime.hashCode();
    }

    public boolean equals(Object field) {
        return ((DateTimeField) field).dateTime.equals(this.dateTime);
    }

    public void serialize(DataOutputStream dos) throws IOException {
        dos.writeLong(this.dateTime.toEpochSecond(ZoneOffset.UTC));
    }
    
    public byte[] toByteArray() {
    	return ByteBuffer.allocate(8).putLong(this.dateTime.toInstant(ZoneOffset.UTC).getEpochSecond()).array();
    }

    /**
     * Compare the specified field to the value of this Field.
     * Return semantics are as specified by Field.compare
     *
     * @throws IllegalCastException if val is not an IntField
     * @see Field#compare
     */
    public boolean compare(RelationalOperator op, Field val) {
        DateTimeField dtVal = (DateTimeField) val;
        int returnInt = this.dateTime.compareTo(dtVal.getValue());

        switch (op) {
        case EQ:
            return (returnInt == 0) ? true:false;
        case NOTEQ:
            return (returnInt != 0) ? true:false;
        case GT:
            return (returnInt > 0)  ? true:false;
        case GTE:
            return (returnInt >= 0) ? true:false;
        case LT:
            return (returnInt < 0)  ? true:false;
        case LTE:
            return (returnInt <= 0) ? true:false;
        }
        return false;
    }

    /**
     * Return the Type of this field.
     * @return Type.INT_TYPE
     */
	public Type getType() {
		return Type.DATETIME;
	}
}
