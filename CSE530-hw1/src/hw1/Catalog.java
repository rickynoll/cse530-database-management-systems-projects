package hw1;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

/**
 * The Catalog keeps track of all available tables in the database and their
 * associated schemas.
 * For now, this is a stub catalog that must be populated with tables by a
 * user program before it can be used -- eventually, this should be converted
 * to a catalog that reads a catalog table from disk.
 */

public class Catalog {
	
	private class Table {
		private String tableName;
		private HeapFile file;
		private String primaryKey;
		//private int id;
		//private String[] fields;
		//private LinkedList<Tuple> tupleList;
		
		public Table(String name, HeapFile file, String primaryKey) {
			this.tableName = name;
			this.file = file;
			this.primaryKey = primaryKey;
//			this.file.initFile(); // Just Added
			//this.id = 0;
			//this.fields = fields;
		}

		public String getTableName() {
			return tableName;
		}

		public void setTableName(String tableName) {
			this.tableName = tableName;
		}

		public HeapFile getFile() {
			return this.file;
		}

		public void setFile(HeapFile hf) {
			this.file = hf;
		}

		public String getPrimaryKey() {
			return primaryKey;
		}

		public void setPrimaryKey(String primaryKey) {
			this.primaryKey = primaryKey;
		}
	}
	
	private ArrayList<Integer> idList;
	private HashMap<Integer, Table> idMap;
	
	//private int idGenerator;
	
    /**
     * Constructor.
     * Creates a new, empty catalog.
     */
    public Catalog() {
    	idList = new ArrayList<Integer>();
    	idMap = new HashMap<Integer, Table>();
    }

    /**
     * Add a new table to the catalog.
     * This table's contents are stored in the specified HeapFile.
     * @param file the contents of the table to add;  file.getId() is the identfier of
     *    this file/tupledesc param for the calls getTupleDesc and getFile
     * @param name the name of the table -- may be an empty string.  May not be null.  If a name conflict exists, use the last table to be added as the table for a given name.
     * @param pkeyField the name of the primary key field
     */
    public void addTable(HeapFile file, String name, String pkeyField) {
    	//file.initFile();
    	Table holder = this.new Table(name, file, pkeyField);
    	idList.add(holder.getFile().getId());
    	idMap.put(holder.getFile().getId(), holder);
    }

    public void addTable(HeapFile file, String name) {
        addTable(file,name,"");
    }

    /**
     * Return the id of the table with a specified name,
     * @throws NoSuchElementException if the table doesn't exist
     */
    public int getTableId(String name) throws NoSuchElementException {
    	Set<Integer> keys = idMap.keySet();
    	for (int key : keys) {
    		if (idMap.get(key).getTableName().toLowerCase().equals(name.toLowerCase())) {
    			return key;
    		}
    	}
    	throw new NoSuchElementException();
    }

    /**
     * Returns the tuple descriptor (schema) of the specified table
     * @param tableid The id of the table, as specified by the DbFile.getId()
     *     function passed to addTable
     */
    public TupleDesc getTupleDesc(int tableid) throws NoSuchElementException {
    	TupleDesc returnTupleDesc = null;
    	try {
    		returnTupleDesc = idMap.get(tableid).getFile().getTupleDesc();
    	} catch (NullPointerException e) {
    		e.printStackTrace();
    	}
    	return returnTupleDesc;
    }

    /**
     * Returns the HeapFile that can be used to read the contents of the
     * specified table.
     * @param tableid The id of the table, as specified by the HeapFile.getId()
     *     function passed to addTable
     */
    public HeapFile getDbFile(int tableid) throws NoSuchElementException {
    	return idMap.get(tableid).getFile();
    	//return tableList.get(tableid).getFile();
    }

    /** Delete all tables from the catalog */
    public void clear() {
    	idList.clear();
    	idMap.clear();
    }

    public String getPrimaryKey(int tableid) {
    	return idMap.get(tableid).getPrimaryKey();
    }

    public Iterator<Integer> tableIdIterator() {
    	return this.idList.iterator();
//    	List<Integer> idList = new ArrayList<Integer>();
//     	for (int i = 0; i < tableList.size(); i++) {
//    		idList.add(tableList.get(i).getFile().getId());
//    	}
//    	return idList.iterator();
    }

    public String getTableName(int id) {
    	return idMap.get(id).getTableName();
    }
    
    /**
     * Reads the schema from a file and creates the appropriate tables in the database.
     * @param catalogFile
     */
    public void loadSchema(String catalogFile) {
        String line = "";
        try {
            BufferedReader br = new BufferedReader(new FileReader(new File(catalogFile)));

            while ((line = br.readLine()) != null) {
                //assume line is of the format name (field type, field type, ...)
                String name = line.substring(0, line.indexOf("(")).trim();
                //System.out.println("TABLE NAME: " + name);
                String fields = line.substring(line.indexOf("(") + 1, line.indexOf(")")).trim();
                String[] els = fields.split(",");
                ArrayList<String> names = new ArrayList<String>();
                ArrayList<Type> types = new ArrayList<Type>();
                String primaryKey = "";
                for (String e : els) {
                    String[] els2 = e.trim().split(" ");
                    names.add(els2[0].trim());
                    if (els2[1].trim().toLowerCase().equals("int"))
                        types.add(Type.INT);
                    else if (els2[1].trim().toLowerCase().equals("string"))
                        types.add(Type.STRING);
                    else if (els2[1].trim().toLowerCase().equals("boolean"))
                    	types.add(Type.BOOLEAN);
                    else if (els2[1].trim().toLowerCase().equals("float"))
                    	types.add(Type.FLOAT);
                    else if (els2[1].trim().toLowerCase().equals("date"))
                    	types.add(Type.DATE);
                    else if (els2[1].trim().toLowerCase().equals("time"))
                    	types.add(Type.TIME);
                    else if (els2[1].trim().toLowerCase().equals("datetime"))
                    	types.add(Type.DATETIME);
                    else {
                        System.out.println("Unknown type " + els2[1]);
                        System.exit(0);
                    }
                    if (els2.length == 3) {
                        if (els2[2].trim().equals("pk"))
                            primaryKey = els2[0].trim();
                        else {
                            System.out.println("Unknown annotation " + els2[2]);
                            System.exit(0);
                        }
                    }
                }
                Type[] typeAr = types.toArray(new Type[0]);
                String[] namesAr = names.toArray(new String[0]);
                TupleDesc t = new TupleDesc(typeAr, namesAr);
                HeapFile tabHf = new HeapFile(new File("testfiles/" + name + ".dat"), t);
                // tabHf.initFile();
                addTable(tabHf,name,primaryKey);
                System.out.println("Added table : " + name + " with schema " + t);
            }
        } catch (IOException e) {
            e.printStackTrace();
            System.exit(0);
        } catch (IndexOutOfBoundsException e) {
            System.out.println ("Invalid catalog entry : " + line);
            System.exit(0);
        }
    }
}

