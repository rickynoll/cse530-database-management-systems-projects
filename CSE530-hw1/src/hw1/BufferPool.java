package hw1;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.concurrent.TimeUnit;

/**
 * BufferPool manages the reading and writing of pages into memory from
 * disk. Access methods call into it to retrieve pages, and it fetches
 * pages from the appropriate location.
 * <p>
 * The BufferPool is also responsible for locking;  when a transaction fetches
 * a page, BufferPool which check that the transaction has the appropriate
 * locks to read/write the page.
 */
public class BufferPool {
    /** Bytes per page, including header. */
    public static final int PAGE_SIZE = 4096;

    /** Default number of pages passed to the constructor. This is used by
    other classes. BufferPool should use the numPages argument to the
    constructor instead. */
    public static final int DEFAULT_PAGES = 2;
    private static final int TIMEOUT_COUNT = 10;
    private int numPages;
//    private HashMap<Integer,Boolean> isDirty;
    private HashMap<Integer,HeapPage> cache; // { HeapPage.id : HeapPage }
    private HashMap<Integer,Integer> deadlockTracker; 	// { transaction_id : times_called }
    													// Timeout at 10 calls
    /**
     * Creates a BufferPool that caches up to numPages pages.
     *
     * @param numPages maximum number of pages in this buffer pool.
     */
    public BufferPool(int numPages) {
        // your code here
    	this.numPages = numPages;
//    	this.isDirty = new HashMap<Integer,Boolean>();
    	this.cache = new HashMap<Integer,HeapPage>();
    	this.deadlockTracker = new HashMap<Integer,Integer>();
    }
    

    /**
     * Retrieve the specified page with the associated permissions.
     * Will acquire a lock and may block if that lock is held by another
     * transaction.
     * <p>
     * The retrieved page should be looked up in the buffer pool.  If it
     * is present, it should be returned.  If it is not present, it should
     * be added to the buffer pool and returned.  If there is insufficient
     * space in the buffer pool, an page should be evicted and the new page
     * should be added in its place.
     *
     * @param tid the ID of the transaction requesting the page
     * @param tableId the ID of the table with the requested page
     * @param pid the ID of the requested page
     * @param perm the requested permissions on the page
     */
    public HeapPage getPage(int tid, int tableId, int pid, Permissions perm)
        throws Exception {
        // First grab the page from the cache list.
    	HeapPage foundPage = this.findPage(tableId, pid);
    	
    	// 2 Cases: requesting either a write lock or a read lock
    	if (perm.permLevel == 0) {
    		// READ_ONLY
    		System.out.println("Adding READ_ONLY lock to page=" + foundPage.getId() + " for transaction=" + tid);
//    		this.cache.get(foundPage.getId()).setReadLockForTransaction(tid);
    		foundPage = this.setReadLock(tid, this.cache.get(foundPage.getId()));
    	}
    	else {
    		// READ_WRITE
    		System.out.println("Adding READ_WRITE lock to page=" + foundPage.getId() + " for transaction=" + tid);
//    		this.cache.get(foundPage.getId()).setWriteLockForTransaction(tid);
    		foundPage = this.setWriteLock(tid, this.cache.get(foundPage.getId()));
    	}
        return this.cache.get(foundPage.getId()); // Return the page from the cache
    }
    
    public HeapPage findPage(int tableId, int pid) throws Exception {
    	HeapPage foundPage = this.cache.get(pid);
    	if (foundPage == null) {
    		foundPage = Database.getCatalog().getDbFile(tableId).readPage(pid);
    		this.addPageToCache(foundPage);
    	}
    	return foundPage;
    }
    
    public void addPageToCache(HeapPage page) throws Exception {
    	if (this.cache.size() < this.numPages) {
    		// if the cache isn't full yet
    		System.out.println("Cache isnt full yet. Adding new page=" + page.getId());
    		this.cache.put(page.getId(), page);
//	    	this.isDirty.put(page.getId(), false);
//	    	System.out.println("Dirtied page=" + page.getId());
    	}
    	else {
    		// if a page has to get evicted
    		System.out.println("Page evicted from cache on insert");
    		this.evictPage();
    		System.out.println("Adding new page=" + page.getId());
    		this.cache.put(page.getId(), page);
//	    	this.isDirty.put(page.getId(), false);
//	    	System.out.println("Dirtied page=" + page.getId());
    	}
    }
    
    public HeapPage setReadLock(int tid, HeapPage page) throws InterruptedException {
//    	boolean finished = false;
//    	while (!finished) {
		Boolean readlock = page.getReadLockForTransaction(tid);
		Boolean writelock = page.getWriteLockForTransaction(tid);
//		System.out.println("writelock: " + writelock);
		if (writelock != null){
			System.out.println("Page=" + page.getId() + " is already READ_WRITE locked for transaction=" + tid);
			// BLOCKING
			TimeUnit.MILLISECONDS.sleep(200);
			if (this.deadlockTracker.get(tid) == null) { // || this.deadlockTracker.get(tid) == 10) {
				this.deadlockTracker.put(tid, 1);
				System.out.println("transaction=" + tid + " requested READ_WRITE lock times=" + this.deadlockTracker.get(tid) + ".");
				return this.setReadLock(tid, this.cache.get(page.getId())); // start recursive blocking
			}
			else if (this.deadlockTracker.get(tid) >= TIMEOUT_COUNT) {
				int timesCalled = this.deadlockTracker.get(tid);
				this.deadlockTracker.put(tid, timesCalled+1);
				System.out.println("transaction=" + tid + " requested READ_WRITE lock times=" + this.deadlockTracker.get(tid) + ". REQUEST TIMEOUT ! ! !");
				System.out.println("HIT TIMEOUT_COUT . . .\n");
				this.deadlockTracker.remove(tid);
				return this.cache.get(page.getId()); // Abort without putting lock on
			}
			else {
//				System.out.println("transaction=" + tid + " tried to request a lock for the " + timesCalled + " time and failed.");
				int timesCalled = this.deadlockTracker.get(tid);
				this.deadlockTracker.put(tid, timesCalled+1);
				System.out.println("transaction=" + tid + " requested READ_WRITE lock times=" + this.deadlockTracker.get(tid) + ".");
				return this.setReadLock(tid, this.cache.get(page.getId()));
			}
			// END BLOCKING
		}
		else if (readlock != null) {
			System.out.println("Page=" + page.getId() + " is already READ_ONLY locked for transaction=" + tid);
			if (page.getreadLocks().size() == 1) {
				System.out.println("Page already READ_ONLY locked for transaction=" + tid + ". ABORTING REQUEST X X X");
				return this.cache.get(page.getId());
			}
			else {
				System.out.println("Setting READ_ONLY lock for transaction=" + tid);
				this.cache.get(page.getId()).setReadLockForTransaction(tid);
				return this.cache.get(page.getId());
			}
		}
		else {
			System.out.println("Setting read lock on page=" + page.getId() + " for transaction=" + tid);
			this.cache.get(page.getId()).setReadLockForTransaction(tid);
			return this.cache.get(page.getId());
		}
    }
    
    public HeapPage setWriteLock(int tid, HeapPage page) throws Exception {
    	boolean finished = false;
//    	while (!finished) {
//    	page = this.findPage(page.getTableId(), page.getId());
		Boolean readlocks = this.cache.get(page.getId()).getReadLockForTransaction(tid);
		Boolean writelock = this.cache.get(page.getId()).getWriteLockForTransaction(tid);
//    		System.out.println("writelock != null -> " + (writelock != null));
//    		System.out.println("readlock  -> " + readlocks);
//    		System.out.println("writelock -> " + writelock);
		if (writelock != null) {
			System.out.println("Page=" + page.getId() + " is already READ_WRITE locked for transaction=" + tid);
		}
		else if (!page.getWriteLock().isEmpty()) {
			// BLOCKING
			TimeUnit.MILLISECONDS.sleep(200);
			if (this.deadlockTracker.get(tid) == null) { // || this.deadlockTracker.get(tid) == 10) {
				this.deadlockTracker.put(tid, 1);
				System.out.println("transaction=" + tid + " requested READ_WRITE lock times=" + this.deadlockTracker.get(tid) + ".");
				return this.setWriteLock(tid, this.cache.get(page.getId())); // start recursive blocking
			}
			else if (this.deadlockTracker.get(tid) >= TIMEOUT_COUNT) {
				int timesCalled = this.deadlockTracker.get(tid);
				this.deadlockTracker.put(tid, timesCalled+1);
				System.out.println("transaction=" + tid + " requested READ_WRITE lock times=" + this.deadlockTracker.get(tid) + ". REQUEST TIMEOUT ! ! !");
				System.out.println("HIT TIMEOUT_COUT . . .\n");
				this.deadlockTracker.remove(tid);
				return this.cache.get(page.getId()); // Abort without putting lock on
			}
			else {
//				System.out.println("transaction=" + tid + " tried to request a lock for the " + timesCalled + " time and failed.");
				int timesCalled = this.deadlockTracker.get(tid);
				this.deadlockTracker.put(tid, timesCalled+1);
				System.out.println("transaction=" + tid + " requested READ_WRITE lock times=" + this.deadlockTracker.get(tid) + ".");
				return this.setWriteLock(tid, this.cache.get(page.getId()));
			}
			// END BLOCKING
		}
		else if (readlocks != null) {
			if (page.getreadLocks().size() == 1) {
				System.out.println("Upgrading READ_ONLY lock to READ_WRITE for page=" + page.getId());
				this.cache.get(page.getId()).removeReadLockForTransaction(tid);
				this.cache.get(page.getId()).setWriteLockForTransaction(tid);
//				finished = true;
			}
			else {
				System.out.println("Waiting to set READ_WRITE lock because page=" + page.getId() + " is already READ_ONLY locked by multiple transactions");
				// BLOCKING
				TimeUnit.MILLISECONDS.sleep(200);
				if (this.deadlockTracker.get(tid) == null) { // || this.deadlockTracker.get(tid) == 10) {
					this.deadlockTracker.put(tid, 1);
					System.out.println("transaction=" + tid + " requested READ_WRITE lock times=" + this.deadlockTracker.get(tid) + ".");
					return this.setWriteLock(tid, this.cache.get(page.getId())); // start recursive blocking
				}
				else if (this.deadlockTracker.get(tid) >= TIMEOUT_COUNT) {
					int timesCalled = this.deadlockTracker.get(tid);
					this.deadlockTracker.put(tid, timesCalled+1);
					System.out.println("transaction=" + tid + " requested READ_WRITE lock times=" + this.deadlockTracker.get(tid) + ". REQUEST TIMEOUT ! ! !");
					System.out.println("HIT TIMEOUT_COUT . . .\n");
					this.deadlockTracker.remove(tid);
					return this.cache.get(page.getId()); // Abort without putting lock on
				}
				else {
//					System.out.println("transaction=" + tid + " tried to request a lock for the " + timesCalled + " time and failed.");
					int timesCalled = this.deadlockTracker.get(tid);
					this.deadlockTracker.put(tid, timesCalled+1);
					System.out.println("transaction=" + tid + " requested READ_WRITE lock times=" + this.deadlockTracker.get(tid) + ".");
					return this.setWriteLock(tid, this.cache.get(page.getId()));
				}
				// END BLOCKING
			}
		}
		else {
			System.out.println("Setting READ_WRITE lock for page=" + page.getId() + " for transaction=" + tid);
			this.cache.get(page.getId()).setWriteLockForTransaction(tid);
			finished = true;
		}
//    	}
		return this.cache.get(page.getId());
    }

    /**
     * Releases the lock on a page.
     * Calling this is very risky, and may result in wrong behavior. Think hard
     * about who needs to call this and why, and why they can run the risk of
     * calling it.
     *
     * @param tid the ID of the transaction requesting the unlock
     * @param tableID the ID of the table containing the page to unlock
     * @param pid the ID of the page to unlock
     */
    public  void releasePage(int tid, int tableId, int pid) throws Exception {
        // your code here
//    	HeapPage page = this.findPage(tableId, pid);
//    	this.cache.get(pid).setDirty(false);
//    	HashMap<Integer,Boolean> readlocks = page.getreadLocks();
//    	Boolean writelock = page.getWriteLockForTransaction(tid);
//    	if (writelock != null) {
//    		if (readlocks == null || page.getreadLocks().size() == 0) {
//    			System.out.println("Deleting READ_WRITE lock for page=" + page.getId() + " and transaction=" + tid);
//    			this.cache.get(page.getId()).removeWriteLockForTransaction(tid);
//    		}
//    		else {
//    			System.out.println("Downgrading READ_WRITE lock to READ_ONLY for page=" + page.getId() + " and transaction=" + tid);
//    			this.cache.get(page.getId()).removeWriteLockForTransaction(tid);
//    			this.cache.get(page.getId()).setReadLockForTransaction(tid);
//    		}
//    	}
//    	else if (readlocks != null) {
//    		System.out.println("Deleting READ_ONLY lock for page=" + page.getId() + " and transaction=" + tid);
//    		this.cache.get(page.getId()).removeReadLockForTransaction(tid);
////    		if (this.cache.get(page.getId()).getreadLocks().size() == 0) {
////    			
////    		}
//    	}
    	HeapPage hp = this.findPage(tableId, pid);
		hp.removeReadLockForTransaction(tid);
		hp.removeWriteLockForTransaction(tid);
//    	if (this.holdsLock(tid, tableId, pid)) {
//    		HeapPage hp = this.findPage(tableId, pid);
//    		hp.removeReadLockForTransaction(tid);
//    		hp.removeWriteLockForTransaction(tid);
//    	}
    }

    /** Return true if the specified transaction has a lock on the specified page */
    public   boolean holdsLock(int tid, int tableId, int pid) throws Exception {
        // your code here
    	HeapPage hp = this.findPage(tableId, pid);
    	Boolean readLock = hp.getReadLockForTransaction(tid);
    	Boolean writeLock = hp.getWriteLockForTransaction(tid);
    	if (readLock != null && readLock == true) {
    		return true;
    	}
    	if (writeLock != null && writeLock == true) {
    		return true;
    	}
        return false;
    }

    /**
     * Commit or abort a given transaction; release all locks associated to
     * the transaction. If the transaction wishes to commit, write
     *
     * @param tid the ID of the transaction requesting the unlock
     * @param commit a flag indicating whether we should commit or abort
     * @throws Exception 
     */
    public   void transactionComplete(int tid, boolean commit)
        throws Exception {
        // your code here
    	if (commit) {
    		System.out.println(" * * * COMMITTING TRANSACTION * * * ");
    		for (Entry<Integer, HeapPage> entry : this.cache.entrySet()) {
    			if (this.holdsLock(tid, entry.getValue().getTableId(), entry.getKey())) {
    				this.releasePage(tid, entry.getValue().getTableId(), entry.getKey());
    				Database.getCatalog().getDbFile(entry.getValue().getTableId()).writePage(this.cache.get(entry.getKey()));
    			}
//        		Boolean readlock = entry.getValue().getReadLockForTransaction(tid);
//        		Boolean writelock = entry.getValue().getWriteLockForTransaction(tid);
//        		if (writelock != null) {
//        			if (readlock != null && readlock == true) {
//        				System.out.println("Removing READ_WRITE lock from page=" + entry.getKey());
//            			this.cache.get(entry.getKey()).removeWriteLockForTransaction(tid);
//            			System.out.println("Removing READ_ONLY locks from page=" + entry.getKey());
//            			this.cache.get(entry.getKey()).removeReadLockForTransaction(tid);
//            		}
//        			else {
//        				System.out.println("Removing READ_WRITE lock from page=" + entry.getKey());
//            			this.cache.get(entry.getKey()).removeWriteLockForTransaction(tid);
//        			}	
//        			Database.getCatalog().getDbFile(entry.getValue().getTableId()).writePage(this.cache.get(entry.getKey()));
//        		}
//        		else if (readlock != null && readlock == true) {
//        			System.out.println("Removing READ_ONLY locks from page=" + entry.getKey());
//        			this.cache.get(entry.getKey()).removeReadLockForTransaction(tid);
//        			Database.getCatalog().getDbFile(entry.getValue().getTableId()).writePage(this.cache.get(entry.getKey()));
//        		}
        	}
    	}
    	else {
    		System.out.println(" @ @ @ ABORTING TRANSACTION @ @ @ ");
    		for (Entry<Integer, HeapPage> entry : this.cache.entrySet()) {
    			System.out.println("Replacing page=" + this.cache.put(entry.getKey(), Database.getCatalog().getDbFile(entry.getValue().getTableId()).readPage(entry.getKey())).getId());
//    			this.cache.get(entry.getKey()).removeWriteLockForTransaction(tid);
//    			this.cache.get(entry.getKey()).removeReadLockForTransaction(tid);
    		}
    	}
    	
    }

    /**
     * Add a tuple to the specified table behalf of transaction tid.  Will
     * acquire a write lock on the page the tuple is added to. May block if the lock cannot 
     * be acquired.
     * 
     * Marks any pages that were dirtied by the operation as dirty
     *
     * @param tid the transaction adding the tuple
     * @param tableId the table to add the tuple to
     * @param t the tuple to add
     */
    public  void insertTuple(int tid, int tableId, Tuple t)
        throws Exception {
        HeapFile table = Database.getCatalog().getDbFile(tableId);
//        System.out.println("Inserting into heapfile=" + table.getId());
        HeapPage dirtyPage = table.getPageToAddTuple(t);
        if (this.cache.get(dirtyPage.getId()).getWriteLockForTransaction(tid) != null) {
        	dirtyPage.addTuple(t);
        	dirtyPage.setDirty(true);
        	System.out.println("Dirtied page=" + dirtyPage.getId());
        }
        else {
        	System.out.println("No READ_WRITE lock on page=" + dirtyPage.getId());
        	return;
        }
//    	HeapPage dirtyPage = this.findPage(tableId, pid);
        HeapPage cachedPage = this.cache.get(dirtyPage.getId());
        // If we're adding a new page to the HeapFile we have to cache it
        if (cachedPage == null) {
        	// if the page is new and not in the cache then add it
        	System.out.println("Adding dirtyPage=" + dirtyPage.getId() + " to cache");
        	this.addPageToCache(dirtyPage);
        }
        else {
        	// else it will just replace the existing HeapPage in the cache
        	System.out.println("Replacing current cached page=" + dirtyPage.getId());
        	this.cache.put(dirtyPage.getId(), dirtyPage);
        }
    }

    /**
     * Remove the specified tuple from the buffer pool.
     * Will acquire a write lock on the page the tuple is removed from. May block if
     * the lock cannot be acquired.
     *
     * Marks any pages that were dirtied by the operation as dirty.
     *
     * @param tid the transaction adding the tuple.
     * @param tableId the ID of the table that contains the tuple to be deleted
     * @param t the tuple to add
     */
    public  void deleteTuple(int tid, int tableId, Tuple t)
        throws Exception {
        // your code here
    	HeapFile table = Database.getCatalog().getDbFile(tableId);
        HeapPage pageToDeleteFrom = table.getPageToDeleteTuple(t);
        this.cache.get(pageToDeleteFrom.getId()).deleteTuple(t);
        this.cache.get(pageToDeleteFrom.getId()).setDirty(true);
        System.out.println("Deleted tuple=" + t + " from tableId=" + tableId);
    }

    private synchronized  void flushPage(int tableId, int pid) throws IOException {
        // your code here
    	Database.getCatalog().getDbFile(tableId).writePage(this.cache.get(pid));
    	System.out.println("Flushed pageId=" + pid);
    }

    /**
     * Discards a page from the buffer pool.
     * Flushes the page to disk to ensure dirty pages are updated on disk.
     */
    private synchronized  void evictPage() throws Exception {
        // your code here
    	for (Entry<Integer, HeapPage> entry : this.cache.entrySet())
		{
    		HeapPage hp = entry.getValue();
    		if (hp.isLocked()) {
    			continue;
    		}
		    if (!hp.isDirty()) {
		    	System.out.println("Evicting pageId=" + entry.getKey() + " . . .");
		    	this.flushPage(hp.getTableId(), entry.getKey());
		    	this.cache.remove(entry.getKey());
//		    	this.isDirty.remove(entry.getKey());
		    	System.out.println("Eviction Complete.");
		    	return;
		    }
		}
    	throw new Exception();
    }

}
