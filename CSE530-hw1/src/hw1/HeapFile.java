package hw1;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

/**
 * A heap file stores a collection of tuples. It is also responsible for managing pages.
 * It needs to be able to manage page creation as well as correctly manipulating pages
 * when tuples are added or deleted.
 * @author Sam Madden modified by Doug Shook
 *
 */
public class HeapFile {
	
	public static final int PAGE_SIZE = 4096;
	private File file;
	private TupleDesc td;
	//private ArrayList<Tuple> tuples;
	private ArrayList<HeapPage> pages;
	
	/**
	 * Creates a new heap file in the given location that can accept tuples of the given type
	 * @param f location of the heap file
	 * @param types type of tuples contained in the file
	 */
	public HeapFile(File f, TupleDesc type) {
		this.file = f;
		this.td = type;
		this.pages = new ArrayList<HeapPage>();
	}
	
	public void initFile() {
		if (pages.size() == 0) {
			for (int i = 0; i < this.getNumPages(); i++) {
				this.pages.add(readPage(i));
			}
		}
	}
	
	public File getFile() {
		this.initFile();
		return this.file;
	}
	
	public TupleDesc getTupleDesc() {
		return this.td;
	}
	
	/**
	 * Creates a HeapPage object representing the page at the given page number.
	 * Because it will be necessary to arbitrarily move around the file, a RandomAccessFile object
	 * should be used here.
	 * @param id the page number to be retrieved
	 * @return a HeapPage at the given page number
	 */
	public HeapPage readPage(int id) {
		try {
			RandomAccessFile raf = new RandomAccessFile(this.file, "r");
			byte[] buffer = new byte[PAGE_SIZE];
			long numBytesForward = id * PAGE_SIZE;
			raf.seek(numBytesForward);
			raf.read(buffer);
			raf.close();
			return new HeapPage(id, buffer, this.getId());
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * Returns a unique id number for this heap file. Consider using
	 * the hash of the File itself.
	 * @return
	 */
	public int getId() {
		return file.hashCode();
	}
	
	/**
	 * Writes the given HeapPage to disk. Because of the need to seek through the file,
	 * a RandomAccessFile object should be used in this method.
	 * @param p the page to write to disk
	 */
	public void writePage(HeapPage p) {
		try {
			RandomAccessFile raf = new RandomAccessFile(this.file, "rw");
			raf.seek(p.getId() * PAGE_SIZE);
			raf.write(p.getPageData());
			raf.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * Adds a tuple. This method must first find a page with an open slot, creating a new page
	 * if all others are full. It then passes the tuple to this page to be stored. It then writes
	 * the page to disk (see writePage)
	 * @param t The tuple to be stored
	 * @return The HeapPage that contains the tuple
	 */
	public HeapPage addTuple(Tuple t) {
		HeapPage openPage = null;
		boolean pageFound = false;
		// Find first open page
		this.initFile();
		for (HeapPage page : pages) {
			for (int i = 0; i < page.getNumSlots(); i++) {
				if (!page.slotOccupied(i)) {
					openPage = page;
					pageFound = true;
					break;
				}
			}
			if (pageFound) {
				break;
			}
		}
		
		if (pageFound) {
			try {
				openPage.addTuple(t);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			writePage(openPage);
		}
		else {
			try {
				openPage = new HeapPage(this.pages.size(), new byte[PAGE_SIZE], getId());
				openPage.addTuple(t);
				this.pages.add(openPage);
				writePage(openPage);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return openPage;
	}
	
	public HeapPage getPageToAddTuple(Tuple t) {
		HeapPage openPage = null;
		boolean pageFound = false;
		// Find first open page
		this.initFile();
		for (HeapPage page : pages) {
			for (int i = 0; i < page.getNumSlots(); i++) {
				if (!page.slotOccupied(i)) {
					openPage = page;
					pageFound = true;
					break;
				}
			}
			if (pageFound) {
				break;
			}
		}
//		System.out.println("openPage.getWriteLockForTransaction(" + tid + ") -> " + openPage.getWriteLockForTransaction(tid));
//		System.out.println("openPage.getWriteLockForTransaction(" + tid + ") != null -> " + (openPage.getWriteLockForTransaction(tid) != null));
		if (pageFound) { // && openPage.getWriteLockForTransaction(tid) != null) {
			try {
				//openPage.addTuple(t);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
//			writePage(openPage);
		}
		else {
			try {
				openPage = new HeapPage(this.pages.size(), new byte[PAGE_SIZE], getId());
				//openPage.addTuple(t);
				//this.pages.add(openPage);
//				writePage(openPage);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return openPage;
	}
	
	/**
	 * This method will examine the tuple to find out where it is stored, then delete it
	 * from the proper HeapPage. It then writes the modified page to disk.
	 * @param t the Tuple to be deleted
	 */
	public void deleteTuple(Tuple t){
		boolean tupleRemoved = false;
		for (int i = 0; i < pages.size(); i++) {
			HeapPage page = pages.get(i);
			Iterator<Tuple> tupleIterator = page.iterator();
			//int slotCount = 0;
			while (tupleIterator.hasNext()) {
				if (tupleIterator.next().equals(t)) {
					try {
						page.deleteTuple(t);
						// Don't Handle removing pages, just keep forever once HeapPage is added
//						if (page.isEmpty()) {
//							pages.remove(i);
//							for (int j = i; j < pages.size(); j++) {
//								
//							}
//						}
						pages.set(i, page);
						writePage(page);
						tupleRemoved = true;
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				if (tupleRemoved) {
					break;
				}
			}
			if (tupleRemoved) {
				break;
			}
		}
		
	}
	
	public HeapPage getPageToDeleteTuple(Tuple t) {
		boolean tupleRemoved = false;
		for (int i = 0; i < pages.size(); i++) {
			HeapPage page = pages.get(i);
			Iterator<Tuple> tupleIterator = page.iterator();
			//int slotCount = 0;
			while (tupleIterator.hasNext()) {
				if (tupleIterator.next().equals(t)) {
					return page;
				}
			}
		}
		return null;
	}
	
	/**
	 * Returns an ArrayList containing all of the tuples in this HeapFile. It must
	 * access each HeapPage to do this (see iterator() in HeapPage)
	 * @return
	 */
	public ArrayList<Tuple> getAllTuples() {
		this.initFile();
		ArrayList<Tuple> tupleList = new ArrayList<Tuple>();
		for (HeapPage page : pages) {
			Iterator<Tuple> tupleIterator = page.iterator();
			while (tupleIterator.hasNext()) {
				Tuple nextVal = tupleIterator.next();
				if (nextVal != null) {
					tupleList.add(nextVal);
				}
			}
		}
		return tupleList;
	}
	
	/**
	 * Computes and returns the total number of pages contained in this HeapFile
	 * @return the number of pages
	 */
	public int getNumPages() {
		return (int) (file.length() / PAGE_SIZE);
	}
}
