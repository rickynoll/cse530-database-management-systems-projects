package hw1;

import java.io.*;
import java.nio.ByteBuffer;
import java.time.*;

/**
 * Instance of Field that stores a single integer.
 */
public class DateField implements Field {
	
	private LocalDate date;

    public LocalDate getValue() {
        return date;
    }

    /**
     * Constructor.
     * 
     * *Note:* Using deprecated Date(int,int,int) constructors because it probably won't
     *	be unsupported by the time I turn this in.
     *
     * Includes primitive bounds checking for values,
	 * does not handle specific 28/30/31 day differences in months
	 * 
	 * @param year value between 0-9999 representing the year
	 * @param month value between 1-12 representing the month
	 * @param day value between 1-31 representing the day
	 * 
	 */
	public DateField(int year, int month, int day) {
        this.date = LocalDate.of(year, month, day);
    }
    
    /**
     * Constructor.
     * 
	 * @param epochMilliseconds milliseconds since 01/01/1970
	 * 
	 */
    public DateField(long epochMilliseconds) {
    	this.date = Instant.ofEpochMilli(epochMilliseconds).atZone(ZoneId.systemDefault()).toLocalDate();
    }
    
    public DateField(byte[] b) {
    	this.date = Instant.ofEpochMilli(java.nio.ByteBuffer.wrap(b).getLong()).atZone(ZoneId.systemDefault()).toLocalDate();
    }

    public String toString() {
        return date.toString();
    }

    public int hashCode() {
    	return date.hashCode();
    }

    public boolean equals(Object field) {
        return ((DateField) field).date.equals(this.date);
    }

    public void serialize(DataOutputStream dos) throws IOException {
        dos.writeLong(this.date.toEpochDay());
    }
    
    public byte[] toByteArray() {
    	return ByteBuffer.allocate(8).putLong(this.date.toEpochDay()).array();
    }

    /**
     * Compare the specified field to the value of this Field.
     * Return semantics are as specified by Field.compare
     *
     * @throws IllegalCastException if val is not an IntField
     * @see Field#compare
     */
    public boolean compare(RelationalOperator op, Field val) {
        DateField dVal = (DateField) val;
        int returnInt = this.date.compareTo(dVal.getValue());

        switch (op) {
        case EQ:
            return (returnInt == 0) ? true:false;
        case NOTEQ:
            return (returnInt != 0) ? true:false;
        case GT:
            return (returnInt > 0)  ? true:false;
        case GTE:
            return (returnInt >= 0) ? true:false;
        case LT:
            return (returnInt < 0)  ? true:false;
        case LTE:
            return (returnInt <= 0) ? true:false;
        }
        return false;
    }

    /**
     * Return the Type of this field.
     * @return Type.INT_TYPE
     */
	public Type getType() {
		return Type.DATE;
	}
}
