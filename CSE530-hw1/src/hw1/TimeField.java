package hw1;

import java.io.DataOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.time.*;

/**
 * Instance of Field that stores a single integer.
 */
public class TimeField implements Field {
	private LocalTime time;

    public LocalTime getValue() {
        return time;
    }

    /**
     * Constructor.
     * 
     * *Note:* Using deprecated Date(int,int,int) constructors because it probably won't
     *	be unsupported by the time I turn this in.
     *
     * Includes primitive bounds checking for values,
	 * does not handle specific 28/30/31 day differences in months
	 * 
	 * @param year value between 0-9999 representing the year
	 * @param month value between 1-12 representing the month
	 * @param day value between 1-31 representing the day
	 * 
	 */
	public TimeField(int hour, int min) {
        this.time = LocalTime.of(hour, min);
    }
	
	public TimeField(int hour, int min, int sec) {
        this.time = LocalTime.of(hour, min, sec);
    }
	
	public TimeField(int hour, int min, int sec, int nano) {
        this.time = LocalTime.of(hour, min, sec, nano);
    }
    
    /**
     * Constructor.
     * 
	 * @param epochNanoseconds nanoseconds since 01/01/1970
	 * 
	 */
    public TimeField(long epochNanoseconds) {
        this.time = LocalTime.ofNanoOfDay(epochNanoseconds);
    }
    
    public TimeField(byte[] b) {
    	this.time = LocalTime.ofNanoOfDay(java.nio.ByteBuffer.wrap(b).getLong());
    }

    public String toString() {
        return time.toString();
    }

    public int hashCode() {
    	return time.hashCode();
    }

    public boolean equals(Object field) {
        return ((TimeField) field).time.equals(this.time);
    }

    public void serialize(DataOutputStream dos) throws IOException {
        dos.writeLong(this.time.toNanoOfDay());
    }
    
    public byte[] toByteArray() {
    	return ByteBuffer.allocate(8).putLong(this.time.toNanoOfDay()).array();
    }

    /**
     * Compare the specified field to the value of this Field.
     * Return semantics are as specified by Field.compare
     *
     * @throws IllegalCastException if val is not an IntField
     * @see Field#compare
     */
    public boolean compare(RelationalOperator op, Field val) {
        TimeField tVal = (TimeField) val;
        int returnInt = this.time.compareTo(tVal.getValue());

        switch (op) {
        case EQ:
            return (returnInt == 0) ? true:false;
        case NOTEQ:
            return (returnInt != 0) ? true:false;
        case GT:
            return (returnInt > 0)  ? true:false;
        case GTE:
            return (returnInt >= 0) ? true:false;
        case LT:
            return (returnInt < 0)  ? true:false;
        case LTE:
            return (returnInt <= 0) ? true:false;
        }
        return false;
    }

    /**
     * Return the Type of this field.
     * @return Type.INT_TYPE
     */
	public Type getType() {
		return Type.TIME;
	}
}
