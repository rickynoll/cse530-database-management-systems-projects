package hw1;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import net.sf.jsqlparser.JSQLParserException;
import net.sf.jsqlparser.expression.Expression;
import net.sf.jsqlparser.expression.operators.relational.EqualsTo;
import net.sf.jsqlparser.parser.CCJSqlParserUtil;
import net.sf.jsqlparser.schema.Column;
import net.sf.jsqlparser.schema.Table;
import net.sf.jsqlparser.statement.Statement;
import net.sf.jsqlparser.statement.select.Join;
import net.sf.jsqlparser.statement.select.PlainSelect;
import net.sf.jsqlparser.statement.select.Select;
import net.sf.jsqlparser.statement.select.SelectExpressionItem;
import net.sf.jsqlparser.statement.select.SelectItem;

public class Query {

	private String q;
	
	public Query(String q) {
		this.q = q;
	}
	
	public Relation execute()  {
		Statement statement = null;
		try {
			statement = CCJSqlParserUtil.parse(q);
		} catch (JSQLParserException e) {
			System.out.println("Unable to parse query");
			e.printStackTrace();
		}
		// Casting select statement
		// Go to api -- use columns SelectItems.getExpression()
		// Dont use toString() to 
		// 1) Decide on order of operations
		//		- dont worry about optimization or heuristics
		//		- look at from (FromItem in api) first then possibly join
		// 2) Sometimes these queries will exist and sometimes they won't
		//		- we wont always join tables
		// 3) Join object, in PlainSelect the .getJoins() will return a list of join objects
		//  Figure out which nodes in the tree apply, get them, then use the nodes to create relations within the table
		// 	Visitor patter
		// accept takes in a node and finds the type of that node
		// the visitor figures out which class the interface is implemented by and 
		// Only need to call getColumn() and getOp()
		// from join select project aggregate rename
		Catalog c = Database.getCatalog();
		ColumnVisitor cv = new ColumnVisitor();
		WhereExpressionVisitor wev = new WhereExpressionVisitor();

		Select selectStatement = (Select) statement;
		PlainSelect sb = (PlainSelect)selectStatement.getSelectBody();
		System.out.println("\n" + sb + "\n");
		
		// FROM CLAUSE
		Table FromTable = (Table) sb.getFromItem();
		int FromTableId = c.getTableId(FromTable.getName());
		Relation r = new Relation(c.getDbFile(FromTableId).getAllTuples(), c.getTupleDesc(FromTableId));
		
		// JOIN CLAUSE
		List<Join> joinList = sb.getJoins();
		if (joinList != null) {
			for (Join join : joinList) {
				System.out.println("\n" + join + "\n");
				System.out.println("join.getOnExpression() " + join.getOnExpression());
				System.out.println("join.getRightItem() " + join.getRightItem());
				Table JoinTable = (Table) join.getRightItem();
				int JoinTableId = c.getTableId(JoinTable.getName());
				Relation joinTable = new Relation(c.getDbFile(JoinTableId).getAllTuples(), c.getTupleDesc(JoinTableId));
				EqualsTo OnExpression = (EqualsTo) join.getOnExpression();
				Column leftCol = (Column) OnExpression.getLeftExpression();
				Column rightCol = (Column) OnExpression.getRightExpression();
				System.out.println("Left Column Table " + leftCol.getTable().getName());
				System.out.println("Left Column Name " + leftCol.getColumnName());
				
//				HeapFile leftColDBFile = c.getDbFile(c.getTableId(leftCol.getTable().getName()));
//				int leftColId = leftColDBFile.getTupleDesc().nameToId(leftCol.getColumnName());
																				// I think this change from the 
				int leftColId = r.getDesc().nameToId(leftCol.getColumnName());	// two commented lines will let us
																				// join 3+ tables
				int rightColId = c.getDbFile(c.getTableId(rightCol.getTable().getName())).getTupleDesc().nameToId(rightCol.getColumnName());				
				
				r = r.join(joinTable, leftColId, rightColId);
			}
		}
		
		// WHERE CLAUSE
		Expression whereClause = sb.getWhere();
		if (whereClause != null) {
			whereClause.accept(wev);
			String columnName = wev.getLeft();
			Field field = wev.getRight();
			int columnId = r.getDesc().nameToId(columnName);
			System.out.println(columnName);
			System.out.println(columnId);
			r = r.select(columnId, wev.getOp(), field);
		}
		
		// SELECT CLAUSE
		List<SelectItem> selectColumns = sb.getSelectItems();
		ArrayList<Integer> fieldIds = new ArrayList<Integer>();
		ArrayList<Integer> renameFieldIDs = new ArrayList<Integer>();
		ArrayList<String> aliases = new ArrayList<String>();
		AggregateOperator aggOp = null;
		boolean containsAggregate = false;
		selectColumns.get(0).accept(cv);
		if (cv.getColumn().equals("*")) {
			Collection<Integer> allFieldIds = new ArrayList<Integer>();
			for (int i = 0; i < r.getDesc().numFields(); i++) {
				allFieldIds.add(i);
			}
			fieldIds.addAll(allFieldIds);
			System.out.println("Added all fields");
			return r.project(fieldIds);
		}
		else {
			for (int i = 0; i < selectColumns.size(); i++) {
				selectColumns.get(i).accept(cv);
				System.out.println(cv.getColumn());
				if (cv.isAggregate()) {
					aggOp = cv.getOp();
					containsAggregate = true;
				}
				fieldIds.add(r.getDesc().nameToId(cv.getColumn()));
			}
		}
		
		// GROUP BY CLAUSE
		if (sb.getGroupByColumnReferences() != null) {
			r = r.project(fieldIds).aggregate(aggOp, true);
			for (int i = 0; i < selectColumns.size(); i++) {
				SelectExpressionItem item = (SelectExpressionItem) selectColumns.get(i);
				item.accept(cv);
				if (item.toString().contains("AS")) {
					renameFieldIDs.add(r.getDesc().nameToId(cv.getColumn()));
					aliases.add(item.getAlias().getName());
				}
			}
			r = r.rename(renameFieldIDs, aliases);
			
		}
		else if (containsAggregate) {
			r = r.project(fieldIds).aggregate(aggOp, false);
			for (int i = 0; i < selectColumns.size(); i++) {
				SelectExpressionItem item = (SelectExpressionItem) selectColumns.get(i);
				item.accept(cv);
				if (item.toString().contains("AS")) {
					renameFieldIDs.add(r.getDesc().nameToId(cv.getColumn()));
					aliases.add(item.getAlias().getName());
				}
			}
			r = r.rename(renameFieldIDs, aliases);
		}
		else {
			r = r.project(fieldIds);
			for (int i = 0; i < selectColumns.size(); i++) {
				SelectExpressionItem item = (SelectExpressionItem) selectColumns.get(i);
				item.accept(cv);
				if (item.toString().contains("AS")) {
					renameFieldIDs.add(r.getDesc().nameToId(cv.getColumn()));
					aliases.add(item.getAlias().getName());
				}
			}
			r = r.rename(renameFieldIDs, aliases);
		}
		return r;
	}
}
