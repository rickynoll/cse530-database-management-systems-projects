package hw1;

import java.io.*;
import java.nio.ByteBuffer;

/**
 * Instance of Field that stores a single integer.
 */
public class BooleanField implements Field {
    private boolean value;

    public boolean getValue() {
        return value;
    }

    /**
     * Constructor.
     *
     * @param i The value of this field.
     */
    public BooleanField(boolean i) {
        value = i;
    }
    
    public BooleanField(byte[] b) {
    	value = b[0] != 0;
    }

    public String toString() {
        return Boolean.toString(value);
    }

    public int hashCode() {
    	Boolean bobj = new Boolean(value);
    	return bobj.hashCode();
    }

    public boolean equals(Object field) {
        return ((BooleanField) field).value == value;
    }

    public void serialize(DataOutputStream dos) throws IOException {
        dos.writeBoolean(value);
    }
    
    public byte[] toByteArray() {
    	return new byte[]{(byte) (value?1:0)};
    }

    /**
     * Compare the specified field to the value of this Field.
     * Return semantics are as specified by Field.compare
     *
     * @throws IllegalCastException if val is not an IntField
     * @see Field#compare
     */
    public boolean compare(RelationalOperator op, Field val) {

        BooleanField bVal = (BooleanField) val;

        switch (op) {
        case EQ:
            return value == bVal.value;
        case NOTEQ:
            return value != bVal.value;
        default:
        	System.out.println("BooleanField cannot be compared with RelationalOperator." + op);
        }

        return false;
    }

    /**
     * Return the Type of this field.
     * @return Type.BOOLEAN_TYPE
     */
	public Type getType() {
		return Type.BOOLEAN;
	}
}
