package hw1;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

/**
 * A class to perform various aggregations, by accepting one tuple at a time
 * @author Doug Shook
 *
 */
public class Aggregator {
	
	private AggregateOperator o;
	private boolean groupBy;
	private TupleDesc td;
	//private ArrayList<Tuple> tuples;
	private HashMap<String,Tuple> groupTupleMap;
	private HashMap<String,Integer> numTuplesAdded;
	private HashMap<String, ArrayList<Field>> seenFieldsForGroup;
	private HashMap<String,Tuple> sumTupleMap;

	public Aggregator(AggregateOperator o, boolean groupBy, TupleDesc td) {
		this.o = o;
		this.groupBy = groupBy;
		this.td = td;
		this.groupTupleMap = new HashMap<String, Tuple>();
		this.numTuplesAdded = new HashMap<String, Integer>();
		this.seenFieldsForGroup = new HashMap<String,ArrayList<Field>>();
		this.sumTupleMap = new HashMap<String,Tuple>();
	}

	/**
	 * Merges the given tuple into the current aggregation
	 * @param t the tuple to be aggregated
	 */
	public void merge(Tuple t) {
		if (this.groupBy) {
			mergeGroupBy(t);
		}
		else {
			mergeWithoutGroupBy(t);
		}
	}
	
	public void mergeGroupBy(Tuple t) {
		switch (o) {
		case MAX:
			if (td.getType(1) == Type.INT) {
				String group = new String();
				if (td.getType(0) == Type.INT){
					int groupInt  = new IntField(t.getField(0)).getValue();
					String groups = new Integer(groupInt).toString();
					String Gr = new String(groups);
					group = Gr;
				}
				else{
					String Gr = new StringField(t.getField(0)).getValue();
					group = Gr;
				}		
				if (groupTupleMap.containsKey(group)) {
					IntField newFieldValue = new IntField(t.getField(1));
					IntField oldFieldValue = new IntField(groupTupleMap.get(group).getField(1));
					if (newFieldValue.compare(RelationalOperator.GT, oldFieldValue)) {
						groupTupleMap.put(group, t);
					}
				}
				else {
					groupTupleMap.put(group, t);
				}
			}
			else {
				System.out.println("Cannot perform MAX() operation on type: " + td.getType(1));
			}
			break;
		case MIN:
			if (td.getType(1) == Type.INT) {
				String group = new String();
				if (td.getType(0) == Type.INT){
					int groupInt  = new IntField(t.getField(0)).getValue();
					String groups = new Integer(groupInt).toString();
					String Gr = new String(groups);
					group = Gr;
				}
				else{
					String Gr = new StringField(t.getField(0)).getValue();
					group = Gr;
				}		
				if (groupTupleMap.containsKey(group)) {
					IntField newFieldValue = new IntField(t.getField(1));
					IntField oldFieldValue = new IntField(groupTupleMap.get(group).getField(1));
					if (newFieldValue.compare(RelationalOperator.LT, oldFieldValue)) {
						groupTupleMap.put(group, t);
					}
				}
				else {
					groupTupleMap.put(group, t);
				}
			}
			else {
				System.out.println("Cannot perform MIN() operation on type: " + td.getType(1));
			}
			break;
		case AVG:
			if (td.getType(1) == Type.INT) {				
				String group = new String();
				if (td.getType(0) == Type.INT){
					int groupInt  = new IntField(t.getField(0)).getValue();
					String groups = new Integer(groupInt).toString();
					String Gr = new String(groups);
					group = Gr;
				}
				else{
					String Gr = new StringField(t.getField(0)).getValue();
					group = Gr;
				}						
				if (groupTupleMap.containsKey(group)) {
					numTuplesAdded.put(group, numTuplesAdded.get(group)+1);
					IntField newFieldValue = new IntField(t.getField(1));
					IntField oldFieldValue = new IntField(sumTupleMap.get(group).getField(1));
					Tuple newSumTuple = new Tuple(this.td);
					Tuple avgTuple = new Tuple(this.td);
					newSumTuple.setField(1, new IntField((newFieldValue.getValue() + oldFieldValue.getValue())).toByteArray());
					sumTupleMap.put(group, newSumTuple);
					avgTuple.setField(0, t.getField(0));
					avgTuple.setField(1, new IntField((newFieldValue.getValue() + oldFieldValue.getValue())/numTuplesAdded.get(group)).toByteArray());
					groupTupleMap.put(group, avgTuple);					
				}
				else {
					groupTupleMap.put(group, t);
					numTuplesAdded.put(group, 1);
					sumTupleMap.put(group, t);
				}
			}
			else {
				System.out.println("Cannot perform AVG() operation on type: " + td.getType(1));
			}
			break;
		// I'm not exactly sure what this counts, if it counts only by group such as the 'Number' column
		// or does it delineate between 'x' many 5's and 'y' many 7's
		// Don't fully understand what's being passed in, is the 0 index in the tuple the group and the
		// 1 index is the thing to compare equality to?
			
			// I think I fixed the issue I expressed above but check my logic
		case COUNT:		
			System.out.println("in count  ");
			if (td.getType(1) == Type.INT) {
				String group = new String();
				if (td.getType(0) == Type.INT){
					int groupInt  = new IntField(t.getField(0)).getValue();
					String groups = new Integer(groupInt).toString();
					String Gr = new String(groups);
					group = Gr;
				}
				else{
					String Gr = new StringField(t.getField(0)).getValue();
					group = Gr;
				}		
				if (groupTupleMap.containsKey(group)) {
					Tuple newCountTuple = new Tuple(this.td);
					newCountTuple.setField(0, t.getField(0));
					newCountTuple.setField(1, groupTupleMap.get(group).getField(1));
					groupTupleMap.put(group, newCountTuple);
					IntField tupleField = new IntField(t.getField(1));
					if (!seenFieldsForGroup.get(group).contains(tupleField)) {
						IntField currentCount = new IntField(groupTupleMap.get(group).getField(1));
						newCountTuple.setField(1, new IntField(currentCount.getValue()+1).toByteArray());
						seenFieldsForGroup.get(group).add(tupleField);
						groupTupleMap.put(group, newCountTuple);
					}
					
				}
				else {
					Tuple newCountTuple = new Tuple(this.td);
					newCountTuple.setField(0, t.getField(0));
					newCountTuple.setField(1, new IntField(1).toByteArray());
					IntField tupleField = new IntField(t.getField(1));
					ArrayList<Field> newGroupSeenArray = new ArrayList<Field>();
					newGroupSeenArray.add(tupleField);
					seenFieldsForGroup.put(group, newGroupSeenArray);
					groupTupleMap.put(group, newCountTuple);
				}
			}
			else if (td.getType(1) == Type.STRING){
				System.out.println("in string count");
				String group = new StringField(t.getField(0)).getValue();
				if (groupTupleMap.containsKey(group)) {
					Tuple newCountTuple = new Tuple(this.td);
					newCountTuple.setField(0, t.getField(0));
					StringField tupleField = new StringField(t.getField(1));
					
					if (!seenFieldsForGroup.get(group).contains(tupleField)) {
						IntField currentCount = new IntField(groupTupleMap.get(group).getField(1));
						newCountTuple.setField(1, new IntField(currentCount.getValue()+1).toByteArray());
						seenFieldsForGroup.get(group).add(tupleField);
						groupTupleMap.put(group, newCountTuple);
					}
					
				}
				else {
					Tuple newCountTuple = new Tuple(this.td);
					newCountTuple.setField(0, t.getField(0));
					newCountTuple.setField(1, new IntField(1).toByteArray());
					StringField tupleField = new StringField(t.getField(1));
					ArrayList<Field> newGroupSeenArray = new ArrayList<Field>();
					newGroupSeenArray.add(tupleField);
					seenFieldsForGroup.put(group, newGroupSeenArray);
					groupTupleMap.put(group, newCountTuple);
				}
			}
			else {
				System.out.println("Cannot perform MAX() operation on type: " + td.getType(1));
			}
			
			break;
		case SUM:
			if (td.getType(1) == Type.INT) {
				String Gr = new String();
				if (td.getType(0) == Type.INT){
					int groupInt  = new IntField(t.getField(0)).getValue();
					String groups = new Integer(groupInt).toString();
					String group  = new String(groups);
					Gr = group;
				}
				else{
					String group = new StringField(t.getField(0)).getValue();
					Gr = group;
				}
				if (groupTupleMap.containsKey(Gr)) {
					IntField newFieldValue = new IntField(t.getField(1));
					IntField oldFieldValue = new IntField(groupTupleMap.get(Gr).getField(1));
					Tuple avgTuple = new Tuple(this.td);
					avgTuple.setField(0, t.getField(0));
					IntField newSum = new IntField((oldFieldValue.getValue() + newFieldValue.getValue()));
					avgTuple.setField(1, newSum.toByteArray());
					groupTupleMap.put(Gr, avgTuple);
				}
				else {
					groupTupleMap.put(Gr, t);
				}
			}
			else {
				System.out.println("Cannot perform SUM() operation on type: " + td.getType(1));
			}
			break;
		default:
			break;
		}
	}
	
	public void mergeWithoutGroupBy(Tuple t) {
		switch (o) {
		case MAX:
			if (td.getType(0) == Type.INT) {
				if (groupTupleMap.isEmpty()) {
					groupTupleMap.put(td.getFieldName(0), t);
					break;
				}
				IntField newFieldValue = new IntField(t.getField(0));
				IntField oldFieldValue = new IntField(groupTupleMap.get(td.getFieldName(0)).getField(0));
				if (newFieldValue.compare(RelationalOperator.GT, oldFieldValue)) {
					groupTupleMap.put(td.getFieldName(0), t);
				}
			}
			else if (td.getType(0) == Type.FLOAT) {
				if (groupTupleMap.isEmpty()) {
					groupTupleMap.put(td.getFieldName(0), t);
					break;
				}
				FloatField newFieldValue = new FloatField(t.getField(0));
				FloatField oldFieldValue = new FloatField(groupTupleMap.get(td.getFieldName(0)).getField(0));
				if (newFieldValue.compare(RelationalOperator.GT, oldFieldValue)) {
					groupTupleMap.put(td.getFieldName(0), t);
				}
			}
			else if (td.getType(0) == Type.DATE) {
				if (groupTupleMap.isEmpty()) {
					groupTupleMap.put(td.getFieldName(0), t);
					break;
				}
				DateField newFieldValue = new DateField(t.getField(0));
				DateField oldFieldValue = new DateField(groupTupleMap.get(td.getFieldName(0)).getField(0));
				if (newFieldValue.compare(RelationalOperator.GT, oldFieldValue)) {
					groupTupleMap.put(td.getFieldName(0), t);
				}
			}
			else if (td.getType(0) == Type.TIME) {
				if (groupTupleMap.isEmpty()) {
					groupTupleMap.put(td.getFieldName(0), t);
					break;
				}
				TimeField newFieldValue = new TimeField(t.getField(0));
				TimeField oldFieldValue = new TimeField(groupTupleMap.get(td.getFieldName(0)).getField(0));
				if (newFieldValue.compare(RelationalOperator.GT, oldFieldValue)) {
					groupTupleMap.put(td.getFieldName(0), t);
				}
			}
			else if (td.getType(0) == Type.DATETIME) {
				if (groupTupleMap.isEmpty()) {
					groupTupleMap.put(td.getFieldName(0), t);
					break;
				}
				DateTimeField newFieldValue = new DateTimeField(t.getField(0));
				DateTimeField oldFieldValue = new DateTimeField(groupTupleMap.get(td.getFieldName(0)).getField(0));
				if (newFieldValue.compare(RelationalOperator.GT, oldFieldValue)) {
					groupTupleMap.put(td.getFieldName(0), t);
				}
			}
			else {
				System.out.println("Cannot perform MAX() on type: " + td.getType(0));
			}
			break;
		case MIN:
			if (td.getType(0) == Type.INT) {
				if (groupTupleMap.isEmpty()) {
					groupTupleMap.put(td.getFieldName(0), t);
					break;
				}
				IntField newFieldValue = new IntField(t.getField(0));
				IntField oldFieldValue = new IntField(groupTupleMap.get(td.getFieldName(0)).getField(0));
				if (newFieldValue.compare(RelationalOperator.LT, oldFieldValue)) {
					groupTupleMap.put(td.getFieldName(0), t);
				}
			}
			else if (td.getType(0) == Type.FLOAT) {
				if (groupTupleMap.isEmpty()) {
					groupTupleMap.put(td.getFieldName(0), t);
					break;
				}
				FloatField newFieldValue = new FloatField(t.getField(0));
				FloatField oldFieldValue = new FloatField(groupTupleMap.get(td.getFieldName(0)).getField(0));
				if (newFieldValue.compare(RelationalOperator.LT, oldFieldValue)) {
					groupTupleMap.put(td.getFieldName(0), t);
				}
			}
			else if (td.getType(0) == Type.DATE) {
				if (groupTupleMap.isEmpty()) {
					groupTupleMap.put(td.getFieldName(0), t);
					break;
				}
				DateField newFieldValue = new DateField(t.getField(0));
				DateField oldFieldValue = new DateField(groupTupleMap.get(td.getFieldName(0)).getField(0));
				if (newFieldValue.compare(RelationalOperator.LT, oldFieldValue)) {
					groupTupleMap.put(td.getFieldName(0), t);
				}
			}
			else if (td.getType(0) == Type.TIME) {
				if (groupTupleMap.isEmpty()) {
					groupTupleMap.put(td.getFieldName(0), t);
					break;
				}
				TimeField newFieldValue = new TimeField(t.getField(0));
				TimeField oldFieldValue = new TimeField(groupTupleMap.get(td.getFieldName(0)).getField(0));
				if (newFieldValue.compare(RelationalOperator.LT, oldFieldValue)) {
					groupTupleMap.put(td.getFieldName(0), t);
				}
			}
			else if (td.getType(0) == Type.DATETIME) {
				if (groupTupleMap.isEmpty()) {
					groupTupleMap.put(td.getFieldName(0), t);
					break;
				}
				DateTimeField newFieldValue = new DateTimeField(t.getField(0));
				DateTimeField oldFieldValue = new DateTimeField(groupTupleMap.get(td.getFieldName(0)).getField(0));
				if (newFieldValue.compare(RelationalOperator.LT, oldFieldValue)) {
					groupTupleMap.put(td.getFieldName(0), t);
				}
			}
			else {
				System.out.println("Cannot perform MIN() on type: " + td.getType(0));
			}
			break;
		case AVG:
			if (td.getType(0) == Type.INT) {
				if (groupTupleMap.isEmpty()) {
					groupTupleMap.put(td.getFieldName(0), t);
					sumTupleMap.put(td.getFieldName(0), t);
					numTuplesAdded.put(td.getFieldName(0),1);
					break;
				}
				numTuplesAdded.put(td.getFieldName(0), numTuplesAdded.get(td.getFieldName(0))+1);
				IntField newField = new IntField(t.getField(0));
				IntField oldField = new IntField(sumTupleMap.get(td.getFieldName(0)).getField(0));
				Tuple newSumTuple = new Tuple(this.td);
				newSumTuple.setField(0, new IntField((newField.getValue() + oldField.getValue())).toByteArray());
				sumTupleMap.put(td.getFieldName(0), newSumTuple);
				Tuple newAvgTuple = new Tuple(this.td);
				newAvgTuple.setField(0, new IntField((newField.getValue() + oldField.getValue())/numTuplesAdded.get(td.getFieldName(0))).toByteArray());
				groupTupleMap.put(td.getFieldName(0), newAvgTuple);
			}
			else {
				System.out.println("Cannot perform AVG() on type: " + td.getType(0));
			}
			break;
		case COUNT:
			if (td.getType(0) == Type.INT) {
				String group = td.getFieldName(0);
				if (groupTupleMap.containsKey(group)) {
					Tuple newCountTuple = new Tuple(this.td);
					IntField tupleField = new IntField(t.getField(0));
					if (!seenFieldsForGroup.get(group).contains(tupleField)) {
						IntField currentCount = new IntField(groupTupleMap.get(group).getField(0));
						newCountTuple.setField(0, new IntField(currentCount.getValue()+1).toByteArray());
						seenFieldsForGroup.get(group).add(tupleField); // might need to put updated array back in map
						groupTupleMap.put(group, newCountTuple);
					}
				}
				else {
					Tuple newCountTuple = new Tuple(this.td);
					newCountTuple.setField(0, new IntField(1).toByteArray());
					IntField tupleField = new IntField(t.getField(0));
					ArrayList<Field> newGroupSeenArray = new ArrayList<Field>();
					newGroupSeenArray.add(tupleField);
					seenFieldsForGroup.put(group, newGroupSeenArray);
					groupTupleMap.put(group, newCountTuple);
				}
			}
			else if (td.getType(0) == Type.STRING) {
				String group = new StringField(t.getField(0)).getValue();
				if (groupTupleMap.containsKey(group)) {
					Tuple newCountTuple = new Tuple(this.td);
					newCountTuple.setField(0, t.getField(0));
					StringField tupleField = new StringField(t.getField(1));
					if (!seenFieldsForGroup.get(group).contains(tupleField)) {
						IntField currentCount = new IntField(groupTupleMap.get(group).getField(1));
						newCountTuple.setField(1, new IntField(currentCount.getValue()+1).toByteArray());
						seenFieldsForGroup.get(group).add(tupleField);
						groupTupleMap.put(group, newCountTuple);
					}
				}
				else {
					Tuple newCountTuple = new Tuple(this.td);
					newCountTuple.setField(0, t.getField(0));
					newCountTuple.setField(1, new IntField(1).toByteArray());
					StringField tupleField = new StringField(t.getField(1));
					ArrayList<Field> newGroupSeenArray = new ArrayList<Field>();
					newGroupSeenArray.add(tupleField);
					seenFieldsForGroup.put(group, newGroupSeenArray);
					groupTupleMap.put(group, newCountTuple);
				}
			}
//			else if (td.getType(0) == Type.BOOLEAN){
//				String group = new BooleanField(t.getField(0)).toString();
//				System.out.println("#$% --> " +group);
//				if (groupTupleMap.containsKey(group)) {
//					Tuple newCountTuple = new Tuple(this.td);
//					newCountTuple.setField(0, t.getField(0));
//					StringField tupleField = new StringField(t.getField(1));
//					if (!seenFieldsForGroup.get(group).contains(tupleField)) {
//						IntField currentCount = new IntField(groupTupleMap.get(group).getField(1));
//						newCountTuple.setField(1, new IntField(currentCount.getValue()+1).toByteArray());
//						seenFieldsForGroup.get(group).add(tupleField);
//						groupTupleMap.put(group, newCountTuple);
//					}
//				}
//				else {
//					Tuple newCountTuple = new Tuple(this.td);
//					newCountTuple.setField(0, t.getField(0));
//					newCountTuple.setField(1, new IntField(1).toByteArray());
//					StringField tupleField = new StringField(t.getField(1));
//					ArrayList<Field> newGroupSeenArray = new ArrayList<Field>();
//					newGroupSeenArray.add(tupleField);
//					seenFieldsForGroup.put(group, newGroupSeenArray);
//					groupTupleMap.put(group, newCountTuple);
//				}
//			}
			else {
				System.out.println("Cannot perform MAX() operation on type: " + td.getType(1));
			}
			break;
		case SUM:
			if (td.getType(0) == Type.INT) {
				if (groupTupleMap.isEmpty()) {
					groupTupleMap.put(td.getFieldName(0), t);
					break;
				}
				IntField newField = new IntField(t.getField(0));
				IntField oldField = new IntField(groupTupleMap.get(td.getFieldName(0)).getField(0));
				Tuple newSumTuple = new Tuple(this.td);
				newSumTuple.setField(0, new IntField((newField.getValue() + oldField.getValue())).toByteArray());
				groupTupleMap.put(td.getFieldName(0), newSumTuple);
			}
			else if (td.getType(0) == Type.FLOAT) {
				if (groupTupleMap.isEmpty()) {
					groupTupleMap.put(td.getFieldName(0), t);
					break;
				}
				FloatField newField = new FloatField(t.getField(0));
				FloatField oldField = new FloatField(groupTupleMap.get(td.getFieldName(0)).getField(0));
				Tuple newSumTuple = new Tuple(this.td);
				newSumTuple.setField(0, new FloatField((newField.getValue() + oldField.getValue())).toByteArray());
				groupTupleMap.put(td.getFieldName(0), newSumTuple);
			}
			else {
				System.out.println("Cannot perform SUM() on type: " + td.getType(0));
			}
			break;
		default:
			break;
		}
	}
	
	/**
	 * Returns the result of the aggregation
	 * @return a list containing the tuples after aggregation
	 */
	public ArrayList<Tuple> getResults() {
		return new ArrayList<Tuple>(this.groupTupleMap.values());
	}

}
