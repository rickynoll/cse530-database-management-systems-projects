package hw1;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.NoSuchElementException;

public class HeapPage {

	private int id;
	private byte[] header;
	private Tuple[] tuples;
	private TupleDesc td;
	private int numSlots;
	private int tableId;
	private HashMap<Integer, Tuple> slotMap;
	private HashMap<Integer,Boolean> readLocks; // Maps: TransactionID -> LockedStatus
	private HashMap<Integer,Boolean> writeLock;			  // Maps: TransactionID -> LockedStatus
	private boolean isDirty;
	//private HashMap<Tuple, Integer> emptyIDs;



	public HeapPage(int id, byte[] data, int tableId) throws IOException {
		this.id = id;
		this.tableId = tableId;
		this.slotMap = new HashMap<Integer, Tuple>();
		this.td = Database.getCatalog().getTupleDesc(this.tableId);
		this.numSlots = getNumSlots();
		this.readLocks = new HashMap<Integer,Boolean>();
		this.writeLock = new HashMap<Integer,Boolean>();
		this.isDirty = false;
		DataInputStream dis = new DataInputStream(new ByteArrayInputStream(data));

		// allocate and read the header slots of this page
		header = new byte[getHeaderSize()];
		for (int i=0; i<header.length; i++) {
			header[i] = dis.readByte();
		}

		try{
			// allocate and read the actual records of this page
			tuples = new Tuple[numSlots];
			for (int i=0; i<tuples.length; i++){
				tuples[i] = readNextTuple(dis,i);
				// Added put
				slotMap.put(i, tuples[i]);
			}
		}catch(NoSuchElementException e){
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		dis.close();
		
	}

	public boolean isDirty() {
		return isDirty;
	}
	
	public void setDirty(boolean isDirty) {
		this.isDirty = isDirty;
	}

	public int getId() {
		return this.id;
	}

	/**
	 * Computes and returns the total number of slots that are on this page (occupied or not).
	 * Must take the header into account!
	 * @return number of slots on this page
	 */
	public int getNumSlots() {
		int pageSizeInBits = HeapFile.PAGE_SIZE * 8;
		int tupleSizeInBits = this.td.getSize() * 8 + 1;
		return pageSizeInBits/tupleSizeInBits;
	}

	/**
	 * Computes the size of the header. Headers must be a whole number of bytes (no partial bytes)
	 * @return size of header in bytes
	 */
	private int getHeaderSize() {  
		return (int) Math.ceil(this.getNumSlots() / 8.0);
	}

	/**
	 * Checks to see if a slot is occupied or not by checking the header
	 * @param s the slot to test
	 * @return true if occupied
	 */
	public boolean slotOccupied(int s) {
		int byteNum = (s / 8);
		int bitNum = (s % 8);
		byte headerByte = this.header[byteNum];
		switch (bitNum) {
		case 0:
			return (0b00000001 & headerByte) != 0;
		case 1:
			return (0b00000010 & headerByte) != 0;
		case 2:
			return (0b00000100 & headerByte) != 0;
		case 3:
			return (0b00001000 & headerByte) != 0;
		case 4:
			return (0b00010000 & headerByte) != 0;
		case 5:
			return (0b00100000 & headerByte) != 0;
		case 6:
			return (0b01000000 & headerByte) != 0;
		case 7:
			return (0b10000000 & headerByte) != 0;
		default:
			return false;
		}
	}

	/**
	 * Sets the occupied status of a slot by modifying the header
	 * @param s the slot to modify
	 * @param value its occupied status
	 */
	public void setSlotOccupied(int s, boolean value) {
		int byteNum = (s / 8);
		int bitNum = (s % 8);
		byte headerByte = this.header[byteNum];
		
		if (value) {
			switch (bitNum) {
			case 0:
				this.header[byteNum] = (byte) (0b00000001 | headerByte);
				break;
			case 1:
				this.header[byteNum] = (byte) (0b00000010 | headerByte);
				break;
			case 2:
				this.header[byteNum] = (byte) (0b00000100 | headerByte);
				break;
			case 3:
				this.header[byteNum] = (byte) (0b00001000 | headerByte);
				break;
			case 4:
				this.header[byteNum] = (byte) (0b00010000 | headerByte);
				break;
			case 5:
				this.header[byteNum] = (byte) (0b00100000 | headerByte);
				break;
			case 6:
				this.header[byteNum] = (byte) (0b01000000 | headerByte);
				break;
			case 7:
				this.header[byteNum] = (byte) (0b10000000 | headerByte);
				break;
			default:
				return;
			}
		} else {
			switch (bitNum) {
			case 0:
				this.header[byteNum] = (byte) (0b11111110 & headerByte);
				break;
			case 1:
				this.header[byteNum] = (byte) (0b11111101 & headerByte);
				break;
			case 2:
				this.header[byteNum] = (byte) (0b11111011 & headerByte);
				break;
			case 3:
				this.header[byteNum] = (byte) (0b11110111 & headerByte);
				break;
			case 4:
				this.header[byteNum] = (byte) (0b11101111 & headerByte);
				break;
			case 5:
				this.header[byteNum] = (byte) (0b11011111 & headerByte);
				break;
			case 6:
				this.header[byteNum] = (byte) (0b10111111 & headerByte);
				break;
			case 7:
				this.header[byteNum] = (byte) (0b01111111 & headerByte);
				break;
			default:
				return;
			}
		}
	}
	
	/**
	 * Adds the given tuple in the next available slot. Throws an exception if no empty slots are available.
	 * Also throws an exception if the given tuple does not have the same structure as the tuples within the page.
	 * @param t the tuple to be added.
	 * @throws Exception
	 */
	public void addTuple(Tuple t) throws Exception {
		boolean headerFull = true;
		if (!(t.getDesc().equals(this.td))) {
			throw new Exception();
		}
		for (int i = 0; i < numSlots; i++) {
			if (!slotOccupied(i)) {
				setSlotOccupied(i, true);
				t.setId(i);
				t.setPid(getId());
				this.tuples[i] = t;
				this.slotMap.put(i, t);
				headerFull = false;
				break;
			}
		}
		if (headerFull) {
			throw new Exception();
		}
	}

	/**
	 * Removes the given Tuple from the page. If the page id from the tuple does not match this page, throw
	 * an exception. If the tuple slot is already empty, throw an exception
	 * @param t the tuple to be deleted
	 * @throws Exception
	 */
	public void deleteTuple(Tuple t) throws Exception{
		if (t.getPid() != this.id) {
			throw new Exception();
		}
		int tupleSlot = -1;
		for (int i = 0; i < tuples.length; i++) {
			if (tuples[i].equals(t)) {
				tupleSlot = i;
				break;
			}
		}
		if (tupleSlot != -1) {
			if (slotOccupied(tupleSlot)) {
				setSlotOccupied(tupleSlot, false);
				tuples[tupleSlot] = null;
				slotMap.remove(tupleSlot);
			}
			else {
				throw new Exception();
			}
		}
		else {
			throw new Exception();
		}
	}
	
	public boolean isEmpty() {
//		boolean isEmpty = true;
		for (int i = 0; i < header.length; i++) {
			if (header[i] != 0) {
				return false;
			}
		}
		return true;
	}
	
	/**
     * Suck up tuples from the source file.
     */
	private Tuple readNextTuple(DataInputStream dis, int slotId) {
		// if associated bit is not set, read forward to the next tuple, and
		// return null.
		if (!slotOccupied(slotId)) {
			
			for (int i=0; i<td.getSize(); i++) {
				try {
					dis.readByte();
				} catch (IOException e) {
					throw new NoSuchElementException("error reading empty tuple");
				}
			}
			return null;
		}

		// read fields in the tuple
		Tuple t = new Tuple(td);
		t.setPid(this.id);
		t.setId(slotId);
		

		for (int j=0; j<td.numFields(); j++) {
			if(td.getType(j) == Type.INT || td.getType(j) == Type.FLOAT) {
				byte[] field = new byte[4];
				try {
					dis.read(field);
					t.setField(j, field);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} else if (td.getType(j) == Type.STRING) {
				byte[] field = new byte[129];
				try {
					dis.read(field);
					t.setField(j, field);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} else if (td.getType(j) == Type.BOOLEAN) {
				byte[] field = new byte[1];
				try {
					dis.read(field);
					t.setField(j, field);
				} catch (IOException e) {
					e.printStackTrace();
				}
			} else if (td.getType(j) == Type.DATE || td.getType(j) == Type.TIME || td.getType(j) == Type.DATETIME) {
				byte[] field = new byte[8];
				try {
					dis.read(field);
					t.setField(j, field);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
//		try {
//			this.addTuple(t);
//		} catch (Exception e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
		this.slotMap.put(slotId, t);
		

		return t;
	}

	/**
     * Generates a byte array representing the contents of this page.
     * Used to serialize this page to disk.
	 *
     * The invariant here is that it should be possible to pass the byte
     * array generated by getPageData to the HeapPage constructor and
     * have it produce an identical HeapPage object.
     *
     * @return A byte array correspond to the bytes of this page.
     */
	public byte[] getPageData() {
		int len = HeapFile.PAGE_SIZE;
		ByteArrayOutputStream baos = new ByteArrayOutputStream(len);
		DataOutputStream dos = new DataOutputStream(baos);

		// create the header of the page
		for (int i=0; i<header.length; i++) {
			try {
				dos.writeByte(header[i]);
			} catch (IOException e) {
				// this really shouldn't happen
				e.printStackTrace();
			}
		}

		// create the tuples
		for (int i=0; i<tuples.length; i++) {

			// empty slot
			if (!slotOccupied(i)) {
				for (int j=0; j<td.getSize(); j++) {
					try {
						dos.writeByte(0);
					} catch (IOException e) {
						e.printStackTrace();
					}

				}
				continue;
			}
			
			for (int j=0; j<td.numFields(); j++) {
				
				byte[] f = tuples[i].getField(j);

				try {
					dos.write(f);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

		// padding
		int zerolen = HeapFile.PAGE_SIZE - (header.length + td.getSize() * tuples.length); //- numSlots * td.getSize();
		byte[] zeroes = new byte[zerolen];
		try {
			dos.write(zeroes, 0, zerolen);
		} catch (IOException e) {
			e.printStackTrace();
		}

		try {
			dos.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return baos.toByteArray();
	}

	/**
	 * Returns an iterator that can be used to access all tuples on this page. 
	 * @return
	 */
	public Iterator<Tuple> iterator() {
		Collection<Tuple> values = this.slotMap.values();
		values.removeIf((Tuple tup) -> tup == null);
		return values.iterator();
	}

	public int getTableId() {
		return tableId;
	}

	public void setTableId(int tableId) {
		this.tableId = tableId;
	}

	public HashMap<Integer, Boolean> getreadLocks() {
		return readLocks;
	}
	
	public Boolean getReadLockForTransaction(int tid) {
		return readLocks.get(tid);
	}

	public void setReadLock(HashMap<Integer, Boolean> readLocks) {
		this.readLocks = readLocks;
	}
	
	public Boolean setReadLockForTransaction(int tid) {
//		Boolean readlocks = this.readLocks.get(tid);
//		if (readlocks == null) {
//			readlocks = new ArrayList<Boolean>();
//			readlocks.add(true);
//		}
//		else {
//			readlocks.add(true);
//		}
//		System.out.println("Setting");
		return this.readLocks.put(tid, true);
	}
	
	public Boolean removeReadLockForTransaction(int tid) {
		return this.readLocks.remove(tid);
	}

	public HashMap<Integer, Boolean> getWriteLock() {
		return writeLock;
	}
	
	public Boolean getWriteLockForTransaction(int tid) {
		return writeLock.get(tid);
	}

	public void setWriteLock(HashMap<Integer, Boolean> writeLock) {
		this.writeLock = writeLock;
	}
	
	public Boolean setWriteLockForTransaction(int tid) {
		return this.writeLock.put(tid, true);
	}
	
	public Boolean removeWriteLockForTransaction(int tid) {
		return this.writeLock.remove(tid);
	}
	
	public Boolean isLocked() {
		return !this.readLocks.isEmpty() && !this.writeLock.isEmpty();
	}
	
}
