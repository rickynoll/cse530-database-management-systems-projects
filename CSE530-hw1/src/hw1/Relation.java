package hw1;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * This class provides methods to perform relational algebra operations. It will be used
 * to implement SQL queries.
 * @author Doug Shook
 *
 */
public class Relation {

	private ArrayList<Tuple> tuples;
	private TupleDesc td;
	
	public Relation(ArrayList<Tuple> l, TupleDesc td) {
		this.tuples = l;
		this.td = td;
	}
	
	/**
	 * This method performs a select operation on a relation
	 * @param field number (refer to TupleDesc) of the field to be compared, left side of comparison
	 * @param op the comparison operator
	 * @param operand a constant to be compared against the given column
	 * @return
	 */
	public Relation select(int field, RelationalOperator op, Field operand) {
		System.out.println("Entered Select");
		//String column_name = td.getFieldName(field);
		ArrayList<Tuple> newRelationTuples = new ArrayList<Tuple>();
		System.out.println(tuples.size());
		for (Tuple tuple : tuples) {
			Type fieldType = td.getType(field);
			System.out.println(fieldType + "FeildType");
			if (fieldType == Type.INT) {
				
				IntField fieldValue = new IntField(tuple.getField(field));
				if (fieldValue.compare(op, operand)) {
					System.out.println(tuple);
					newRelationTuples.add(tuple);
				}
			}
			else if (fieldType == Type.STRING) {
				StringField fieldValue = new StringField(tuple.getField(field));
				if (fieldValue.compare(op, operand)) {
					System.out.println(tuple);
					newRelationTuples.add(tuple);
				}
			}
			else if (fieldType == Type.BOOLEAN) {
				BooleanField fieldValue = new BooleanField(tuple.getField(field));
				if (fieldValue.compare(op, operand)) {
					System.out.println(tuple);
					newRelationTuples.add(tuple);
				}
			}
			else if (fieldType == Type.FLOAT) {
				FloatField fieldValue = new FloatField(tuple.getField(field));
				if (fieldValue.compare(op, operand)) {
					System.out.println(tuple);
					newRelationTuples.add(tuple);
				}
			}
			else if (fieldType == Type.DATE) {
				DateField fieldValue = new DateField(tuple.getField(field));
				if (fieldValue.compare(op, operand)) {
					System.out.println(tuple);
					newRelationTuples.add(tuple);
				}
			}
			else if (fieldType == Type.TIME) {
				TimeField fieldValue = new TimeField(tuple.getField(field));
				if (fieldValue.compare(op, operand)) {
					System.out.println(tuple);
					newRelationTuples.add(tuple);
				}
			}
			else if (fieldType == Type.DATETIME) {
				DateTimeField fieldValue = new DateTimeField(tuple.getField(field));
				if (fieldValue.compare(op, operand)) {
					System.out.println(tuple);
					newRelationTuples.add(tuple);
				}
			}
		}
		
		return new Relation(newRelationTuples, this.td);
	}
	
	/**
	 * This method performs a rename operation on a relation
	 * @param fields the field numbers (refer to TupleDesc) of the fields to be renamed
	 * @param names a list of new names. The order of these names is the same as the order of field numbers in the field list
	 * @return
	 */
	public Relation rename(ArrayList<Integer> fields, ArrayList<String> names) {
		Relation newRelation = this;
		for (int i = 0; i < fields.size(); i++) {
			newRelation.td.setFieldName(fields.get(i), names.get(i));
		}
		return newRelation;
	}
	
	/**
	 * This method performs a project operation on a relation
	 * @param fields a list of field numbers (refer to TupleDesc) that should be in the result
	 * @return
	 */
	//we are ending up w only the tuple[fields] as the output here
	//we need to be looping through all the tuples, restricting the fields, adding that tuple
	//example: it test on field = [1]
	//our output is the singluar tuple, in its original form, in position 1 of this.tuples
	public Relation project(ArrayList<Integer> fields) {
		System.out.println("Project");
		//System.out.println("these tuples " + this.tuples);//added
		Type[] newTypes = new Type[fields.size()];
		String[] newFields = new String[fields.size()];
		ArrayList<Tuple> newTuples = new ArrayList<Tuple>();		
		for (int i = 0; i< fields.size(); i++){
			newTypes[i] = this.td.getType(fields.get(i));
			newFields[i] = this.td.getFieldName(fields.get(i));
			//System.out.println("this tuple for field(i) " + this.tuples.get(fields.get(i)));
			//newTuples.add(this.tuples.get(fields.get(i)));			
		}
		for (Tuple tuple:tuples){
			Tuple T = new Tuple(new TupleDesc(newTypes,newFields));
			for (int j=0;j<fields.size();j++){
				T.setField(j, tuple.getField(fields.get(j)));
			}
			newTuples.add(T);
		}
		System.out.println("new tuples " + newTuples);//added
		return new Relation(newTuples, new TupleDesc(newTypes, newFields));
	}
	
	/**
	 * This method performs a join between this relation and a second relation.
	 * The resulting relation will contain all of the columns from both of the given relations,
	 * joined using the equality operator (=)
	 * @param other the relation to be joined
	 * @param field1 the field number (refer to TupleDesc) from this relation to be used in the join condition
	 * @param field2 the field number (refer to TupleDesc) from other to be used in the join condition
	 * @return
	 */
	public Relation join(Relation other, int field1, int field2) {
		int newNumColumns = this.td.numFields() + other.td.numFields();
		Type[] newTupleDescTypeArray = new Type[newNumColumns];
		String[] newTupleDescFieldArray = new String[newNumColumns];
		ArrayList<Tuple> newRelationTuples = new ArrayList<Tuple>();
		// Assign new Type metadata
		for (int i = 0; i < this.td.numFields(); i++) {
			newTupleDescTypeArray[i] = this.td.getType(i);
			newTupleDescFieldArray[i] = this.td.getFieldName(i);
		}
		// Assign new Field Name metadata
		for (int i = 0; i < other.td.numFields(); i++) {
			newTupleDescTypeArray[this.td.numFields() + i] = other.td.getType(i);
			newTupleDescFieldArray[this.td.numFields() + i] = other.td.getFieldName(i);
		}
		TupleDesc newTupleDesc = new TupleDesc(newTupleDescTypeArray, newTupleDescFieldArray);
		// Join Rows
		for (Tuple thisTuple : this.tuples) {
			for (Tuple otherTuple : other.tuples) {
				if (Arrays.equals(thisTuple.getField(field1), otherTuple.getField(field2))) {
					Tuple newTuple = new Tuple(newTupleDesc);
					for (int i = 0; i < this.td.numFields(); i++) {
						newTuple.setField(i, thisTuple.getField(i));
					}
					for (int i = 0; i < other.td.numFields(); i++) {
						newTuple.setField(this.td.numFields() + i, otherTuple.getField(i));
					}
					newRelationTuples.add(newTuple);
				}
			}
		}
		return new Relation(newRelationTuples, newTupleDesc);
	}
	
	/**
	 * Performs an aggregation operation on a relation. See the lab write up for details.
	 * @param op the aggregation operation to be performed
	 * @param groupBy whether or not a grouping should be performed
	 * @return
	 */
	public Relation aggregate(AggregateOperator op, boolean groupBy) {
		Aggregator agg = new Aggregator(op, groupBy, this.td);
		for (Tuple tuple : tuples) {
			agg.merge(tuple);
		}
		ArrayList<Tuple> aggregatedTuples = agg.getResults();
		return new Relation(aggregatedTuples, this.td);
	}
	
	public TupleDesc getDesc() {
		return this.td;
	}
	
	public ArrayList<Tuple> getTuples() {
		return this.tuples;
	}
	
	/**
	 * Returns a string representation of this relation. The string representation should
	 * first contain the TupleDesc, followed by each of the tuples in this relation
	 */
	public String toString() {
		String returnString = "";
		returnString += this.td.toString() + "\n";
		for (Tuple tulpe : this.tuples) {
			returnString += tulpe.toString() + " ";
		}
		return returnString;
	}
}
