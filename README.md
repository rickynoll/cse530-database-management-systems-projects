# README #

This README documents the Extra Credit Homework 5 assignment completed only by Ricky Noll. (Daniel decided not to do an extra credit assignment)

Overview
========

I wanted to get full functionality for the following 5 new data types into my database . . .

  * BOOLEAN
  * FLOAT
  * DATE
  * TIME
  * DATETIME

### How was this accomplished? ###

1. I created a TypeField class for every type (i.e. BooleanField, DateField)

2. Type Enum
	* All new types were added to the Type enum
	
3. TupleDesc
	* I adapted the TupleDesc.getSize() method to handle the new type sizes:
		* BOOLEAN  -> 1 Byte
		* FLOAT    -> 4 Bytes
		* DATE     -> 8 Bytes
		* TIME     -> 8 Bytes
		* DATETIME -> 8 Bytes
		
4. Tuple
	* I adapted the Tuple.equals(Object) method to use the Field wrapper classes in the equality comparison instead of comparing the raw bytes
	* I modified the Tuple.toString() method to print out the new types
	
5. Catalog
	* Added extra type checking to Catalog.loadSchema(String) so that columns of the new types can
    	be created in tables
		
6. HeapPage
	* Added extra type checking to HeapPage.readNextTuple(DataInputStream,int) so all the data types 
    	could be read from file. (Didn't help me in the long run as I couldn't get my own *.dat and 
		the *.dat.bak files backing up like they did in the other tests you wrote)
		
7. Aggregator
	* This one was a doozy. I only added code in my helper method for the aggregates with no group by
    	(Aggregator.mergeWithoutGroupBy(Tuple)), and only for the following functionality . . .
		* BOOLEAN  -> NONE
		* FLOAT    -> MAX(), MIN(), SUM() (NEGLECTED EASY IMPLEMENTATION OF COUNT() AND AVG() BUT TOO LATE NOW)
   		* DATE     -> MAX(), MIN(), ATTEMPTED COUNT()
   		* TIME     -> MAX(), MIN()
   		* DATETIME -> MAX(), MIN()


### JUnit Test Info ###

** Very Important! **
I wasn't able to get the backup files to sync each time you ran a test in the setup() method so each
time you run the test from the top you have to start with a clean B.dat file. Sorry, I know its annoying.

1. Very confident of implementation for testWrite(), testRemove(), testSimple(), testProject(), testSelect(), 
   testSelectAll(), testAS()
2. Less confident of implementation for aggregates--the MIN/MAX functions should work but I'm not sure beyond
   that point and the test is sadly not very extensive
3. The join is somewhat sensible if you look at the output--it joins the correct keys but duplicates the
   primary key in the new tuple and doesn't seem to return the correct schema


### Conclusion ###

Overall I'm happy with what I integrated. Even though I was hoping to get a little further, I feel like I
spent exactly the right amount of time on it versus studying for the exam along with all my other class work.

** End Note: ** I realized when I hit the Aggregator description as I was writing this readme that I had left
the aggregator test commented out. I ended up submitting that around 12:20am, which is after the deadline. I
would appreciate it if you took that commit as the one to be graded, but I understand if you don't as it was
after the deadline.

p.s. Thanks for an awesome semester! This class solidified my decision to continue to grad school when the
time comes because, like I'm sure you do, I just find this stuff fascinating.